-define(Column(Title, Controls), {column, Title, Controls}).
-define(Section(Title, Controls), {section, Title, Controls}).

-define(Radio(Title, Choices),          {radio, Title, Choices}).
-define(Text(Title, Default),           {text, Title, Default}).
-define(CheckBox(Title, Choices),       {multi_sel, Title, Choices}).
-define(ListBox(Title, Choices),        {list_box, Title, Choices}).
-define(Spin(Title, Default, Min, Max), {spin, Title, Default, Min, Max}).
