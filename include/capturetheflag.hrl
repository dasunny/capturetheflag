-include_lib("interactive_erlang/include/iuniverse.hrl").
-include_lib("interactive_erlang/include/iworld.hrl").
-include_lib("interactive_erlang/include/idictionary.hrl").
-include_lib("interactive_erlang/include/idraw.hrl").
-include_lib("interactive_erlang/include/imenu.hrl").
-include_lib("interactive_erlang/include/iterminal.hrl").

-include("pane_toggle.hrl").
-include("trigger_resize.hrl").

-define(FPS, 20).

-define(PANE_CHOOSING, pane_choosing).
-define(PANE_ARENA,    pane_arena).
-define(PANE_HUD,      pane_hud).
-define(PANE_STATUS,   pane_status).
-define(PANE_PREGAME,  pane_pregame).
-define(PANE_POSTGAME, pane_postgame).

-define(GAMEWIDTH,    1200).
-define(GAMEHEIGHT,   600).

-define(FLAG_HOME, flag_home).
-define(FLAG_DROP, flag_drop).
-define(FLAG_POSS, flag_poss).

-define(PERK_BARRIER, perk_barrier).
-define(PERK_PAROLE,  perk_parole).
-define(PERK_RADAR,   perk_radar).
-define(PERK_SPRINT,  perk_sprint).
-define(PERK_STEALTH, perk_stealth).
-define(PERK_VAULT,   perk_vault).

-define(PHASE_CHOOSING, choosing).
-define(PHASE_PREGAME,  pregame).
-define(PHASE_INGAME,   playing).
-define(PHASE_POSTGAME, postgame).

-define(PLAYER_TYPE_CLIENT, player_client).
-define(PLAYER_TYPE_SERVER, player_server).
-define(PLAYER_TYPE_OTHER,  player_other).

-define(ANIM_PHASE_1, anim_phase_1).
-define(ANIM_PHASE_2, anim_phase_2).

-define(NEW(Update), utilities:new(?MODULE, Update)).
-define(UPDATE,  fun utilities:update/2).
-define(CLEANUP, fun utilities:cleanup/1).
-define(EXPAND,  fun utilities:expand/1).
-define(COLLAPSE, fun utilities:collapse/1).

-define(GETGAMERULE(Key, Default), ?Get({ctf_gamerule, Key}, Default)).
-define(GETGAMERULE(Key), ?GETGAMERULE(Key, undefined)).
-define(SETGAMERULE(Key, Value), ?Set({ctf_gamerule, Key}, Value)).

-define(ISBETWEEN(Lower, X, Upper), (Lower =< X) andalso (X =< Upper)).
-define(ISNEGATIVE(Value), Value < 0).
-define(ISPOSITIVE(Value), Value >= 0).
-define(IFTHENELSE(If, Then, Else), if If -> Then; true -> Else end).

-record(universe, {gamedata,
                   players = [],
                   phase}).

-record(world, {gamedata,
                messages,
                mouse,
                otherplayers,
                player,
                phase,
                arena_size}).

-record(player, {animation,
                 barrier,
                 has_flag,
                 is_invisible,
                 is_jailed,
                 is_radar,
                 is_sprinting,
                 look_angle,
                 mouse_difference,
                 move_direction,
                 phase,
                 player_id,
                 player_name,
                 player_perk,
                 player_position,
                 player_score,
                 player_team,
                 player_type,
                 respawn_position,
                 timer_exhaustion,
                 timer_invisibility,
                 timer_jail,
                 timer_reach,
                 timer_sprint}).

-record(mouse, {position,
                left_down,
                right_down}).

-record(barrier, {dim_height,
                  dim_width,
                  do_draw,
                  is_active,
                  is_immortal,
                  max_n_w_reach,
                  max_s_e_reach,
                  orientation,
                  phase,
                  position,
                  ref,
                  shape,
                  team,
                  timer,
                  vault_type}).

-record(gamedata, {flag_blue,
                   flag_red,
                   limit_capture,
                   limit_time,
                   loaded_map,
                   map_data,
                   map_list,
                   score_blue,
                   score_red}).

-record(gamerule, {health_barrier,
                   health_flag,
                   health_invis,
                   health_sprint,
                   immortal_barriers,
                   invis_max_health,
                   invis_min_health,
                   limit_capture,
                   limit_time,
                   max_exhaustion_time,
                   reach,
                   speed_sprint,
                   speed_walk,
                   time_jail,
                   time_parole}).

-record(flag, {home,
               position,
               state,
               team,
               timer,
               who_has}).

-record(zone, {effect,
               shape,
               shape_data,
               team}).

-record(idmail, {mail,
                 id}).

-record(message, {id,
                  text,
                  timer}).

-record(score, {captures,
                pre_captures,
                same_saves,
                opp_saves,
                tags}).

-record(ctf_timer, {value,
                    value_max,
                    value_min,
                    is_countdown,
                    step,
                    can_expire,
                    is_expired,
                    is_paused}).

-record(map_data, {field,
                   home_blue,
                   home_red,
                   name,
                   size,
                   spawns_blue,
                   spawns_red,
                   version,
                   zones}).

-record(animation, {timer_walk_phase,
                    timer_walk_reset,
                    walk_phase}).

-record(render_options, {client_team,
                         displace_fun,
                         displacement,
                         radar_on}).

-define(DISPLACE(RenderOpts), RenderOpts#render_options.displace_fun).
-define(RADAR_ON(RenderOpts), RenderOpts#render_options.radar_on).
