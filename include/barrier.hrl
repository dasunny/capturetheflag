-define(BARRIER_VAULT_ALL,  vault_all).
-define(BARRIER_VAULT_NONE, vault_none).
-define(BARRIER_VAULT_BLUE, vault_blue).
-define(BARRIER_VAULT_RED,  vault_red).

-define(BARRIER_SHAPE_SQUARE,   barrier_square).
-define(BARRIER_SHAPE_TRIANGLE, barrier_triangle).
-define(BARRIER_SHAPE_CIRCLE,   barrier_circle).

-define(BARRIER_ORIENTATION_NS, north_south).
-define(BARRIER_ORIENTATION_EW, east_west).
-define(BARRIER_ORIENTATION_NE, north_east).
-define(BARRIER_ORIENTATION_SE, south_east).
-define(BARRIER_ORIENTATION_SW, south_west).
-define(BARRIER_ORIENTATION_NW, north_west).

-define(BARRIER_PHASE_EXPANDING, phase_expanding).
-define(BARRIER_PHASE_STATIC,    phase_static).
