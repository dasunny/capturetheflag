-module(combo).

-compile(export_all).


start() ->
  Seq = [[A,B,C,D,1,F,G,H,I] || A <- [0,1],
                                B <- [0,1],
                                C <- [0,1],
                                D <- [0,1],
                                F <- [0,1],
                                G <- [0,1],
                                H <- [0,1],
                                I <- [0,1], (B==1 orelse D==1 orelse F==1 orelse H==1) andalso is_good([A,B,C,D,1,F,G,H,I])],
  io:format("Starting with ~p combinations\n",[length(Seq)]),
  Fun = fun(Elem, Acc) -> case not is_member(Elem, Acc) of
                            true  -> [Elem | Acc];
                            false -> Acc
                          end end,
  Final = lists:foldr(Fun, [], Seq),
  io:format("Ending with ~p combinations\n",[length(Final)]),
  lists:foreach(fun(E) -> io:format("~p~p~pn~p~p~pn~p~p~pn\n",E) end, Final).

is_member(Elem, List) ->
  lists:member(Elem, List) orelse
  lists:member(rotate(Elem), List) orelse
  lists:member(rotate(rotate(Elem)), List) orelse
  lists:member(rotate(rotate(rotate(Elem))), List).

remove_duplicates(Elem, List) ->
  case lists:member(Elem, List) of
    true  -> lists:delete(Elem, List);
    false -> List
  end.

rotate([A,B,C,
        D,E,F,
        G,H,I]) ->
  [G,D,A,
   H,E,B,
   I,F,C].

is_good([1,0,_,
         0,_,_,
         _,_,_]) -> false;
is_good([_,0,1,
         _,_,0,
         _,_,_]) -> false;
is_good([_,_,_,
         _,_,0,
         _,0,1]) -> false;
is_good([_,_,_,
         0,_,_,
         1,0,_]) -> false;
is_good(_) -> true.
