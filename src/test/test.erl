-module(test).

-include_lib("eunit/include/eunit.hrl").
-include("capturetheflag.hrl").

-export([command/1, command/2]).

command(_Object) -> {set, {2, newvalue}}.
command({test, Value1, _Value2}, Arg1) -> {set, {3, Value1 + Arg1}}.

update_test() ->
  %% undefined
  %% {inc, Key}
  %% {inc, {Key, Value}}
  %% {dec, Key}
  %% {dec, {Key, Value}}
  %% {'not', Key}
  %% {add_to_list, {Key, Value}}
  %% {set, {Key, Value}}
  %% {reset, Key}
  %% {unset, Key}
  %% {id_update, {{KeyLoc, KeyValue}, Update}}
  %% {id_delete, {KeyLoc, KeyValue}}
  %% {ie_set, {Key, Value}}
  %% {Field, SubUpdate} when is_integer(Field)
  %% {many, Updates}
  %% {command, Command}
  %% _BadUpdate
  ?assertEqual(object, ?UPDATE(object, undefined)),
  ?assertEqual({type, 2, 2}, ?UPDATE({type, 1, 2}, {inc, 2})),
  ?assertEqual({type, 3, 2}, ?UPDATE({type, 1, 2}, {inc, {2, 2}})),
  ?assertEqual({type, 0, 2}, ?UPDATE({type, 1, 2}, {dec, 2})),
  ?assertEqual({type, -1, 2}, ?UPDATE({type, 1, 2}, {dec, {2, 2}})),
  ?assertEqual({type, 1, true}, ?UPDATE({type, 1, false}, {'not', 3})),
  ?assertEqual({type, [obj4, obj1, obj2, obj3]}, ?UPDATE({type, [obj1, obj2, obj3]}, {add_to_list, {2, obj4}})),
  ?assertEqual({type, newvalue1, value2}, ?UPDATE({type, value1, value2}, {set, {2, newvalue1}})),
  ?assertEqual({type, 0, value2}, ?UPDATE({type, value1, value2}, {reset, 2})),
  ?assertEqual({type, undefined, value2}, ?UPDATE({type, value1, value2}, {unset, 2})),
  ?assertEqual([{obj1, 1, newval1},
                {obj2, 2, val2},
                {obj3, 3, val3}], ?UPDATE([{obj1, 1, val1},
                                           {obj2, 2, val2},
                                           {obj3, 3, val3}], {id_update, {{2, 1}, {set, {3, newval1}}}})),
  ?assertEqual([{obj2, 2, val2},
                {obj3, 3, val3}], ?UPDATE([{obj1, 1, val1},
                                           {obj2, 2, val2},
                                           {obj3, 3, val3}], {id_delete, {2, 1}})),
  ?assertEqual({type, [{obj1, 1, newval1},
                       {obj2, 2, val2},
                       {obj3, 3, val3}]}, ?UPDATE({type, [{obj1, 1, val1},
                                                          {obj2, 2, val2},
                                                          {obj3, 3, val3}]}, {2, {id_update, {{2, 1}, {set, {3, newval1}}}}})),
  ?assertEqual({type, [{obj2, 2, val2},
                       {obj3, 3, val3}]}, ?UPDATE({type, [{obj1, 1, val1},
                                                          {obj2, 2, val2},
                                                          {obj3, 3, val3}]}, {2, {id_delete, {2, 1}}})),
  ?assertEqual({type1,
                {type2, newvalue1, value2},
                {type3, value3, value4, value5}}, ?UPDATE({type1,
                                                           {type2, value1, value2},
                                                           {type3, value3, value4, value5}}, {2, {set, {2, newvalue1}}})),
  ?assertEqual({type1,
                {type2, newvalue1, value2},
                {type3, value3, newvalue4, value5}}, ?UPDATE({type1,
                                                              {type2, value1, value2},
                                                              {type3, value3, value4, value5}}, {many, [{2, {set, {2, newvalue1}}},
                                                                                                        {3, {set, {3, newvalue4}}},
                                                                                                        undefined]})),
  ?assertEqual({test, newvalue, value2}, ?UPDATE({test, value, value2}, {command, command})),
  ?assertEqual({test, 1, 4}, ?UPDATE({test, 1, 2}, {command, {command, [3]}})),
  ?assertThrow({error, {bad_update, {{type, value1, value2}, {bad, update}}}}, ?UPDATE({type, value1, value2}, {bad, update})),
  ok.

cleanup_test() ->
  Update = {many, [{gamedata, {many, [{flag_blue, {many,[{many, []}]}},
                                      {flag_red,  {many,[]}},
                                      {set, {limit_time, 5682}}]}},
                   {player, {id_update, {{player_id, iworld},
                                         {many, [{command, {move, [{posn,127,236}, undefined]}},
                                                 undefined]}}}}]},
  Clean = {many, [{gamedata, {set, {limit_time, 5682}}},
                  {player, {id_update, {{player_id, iworld},
                                        {command, {move, [{posn,127,236}, undefined]}}}}}]},
  ?assertEqual(Clean, ?CLEANUP(Update)),
  ?assertEqual({set, {key, value}}, ?CLEANUP({many, [{set, {key, value}}]})),
  ?assertEqual({set, {key, value}}, ?CLEANUP({many, [{set, {key, value}},
                                                     undefined]})),
  ?assertEqual(undefined, ?CLEANUP({many, []})),
  ?assertEqual({key, {set, {key, value}}}, ?CLEANUP({key, {many, [{set, {key, value}}]}})),
  ?assertEqual(undefined, ?CLEANUP({key, {many, [undefined,
                                                 undefined]}})).

expand_test() ->
  Collapsed = {many, [{gamedata, {many, [{flag_blue, {many, [{set, {key1, value1}},
                                                             {set, {key2, value2}}]}},
                                         {flag_red,  {many, [{set, {key3, value3}},
                                                             {set, {key4, value4}}]}},
                                         {set, {limit_time, 5682}}]}},
                      {player, {many, [{id_update, {{player_id, iworld1},
                                                    {many, [{command, {move, [{posn,127,236}, undefined]}},
                                                            {set, {barrier, new_barrier}},
                                                            {barrier, {many, [{timer, {set, {key, newvalue}}},
                                                                              {set, {is_active, false}}]}}]}}},
                                       {id_update, {{player_id, iworld2},
                                                    {many, [{set, {perk, stealth}},
                                                            {set, {player_name, new_player}}]}}}]}}]},
  Expanded = [{gamedata, {flag_blue, {set, {key1, value1}}}},
              {gamedata, {flag_blue, {set, {key2, value2}}}},
              {gamedata, {flag_red, {set, {key3, value3}}}},
              {gamedata, {flag_red, {set, {key4, value4}}}},
              {gamedata, {set, {limit_time, 5682}}},
              {player, {id_update, {{player_id, iworld1},
                                    {barrier, {set, {is_active, false}}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {barrier, {timer, {set, {key, newvalue}}}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {command, {move, [{posn,127,236}, undefined]}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {set, {barrier, new_barrier}}}}},
              {player, {id_update, {{player_id, iworld2},
                                    {set, {perk, stealth}}}}},
              {player, {id_update, {{player_id, iworld2},
                                    {set, {player_name, new_player}}}}}],
  %%   ?debugFmt("\n\n~p\n\n",[?EXPAND(Collapsed)]),
  ?assertEqual(Expanded, ?EXPAND(Collapsed)),
  ?assertEqual([], ?EXPAND(undefined)).

collapse_test() ->
  Expanded = [{gamedata, {flag_blue, {set, {key1, value1}}}},
              {gamedata, {flag_blue, {set, {key2, value2}}}},
              {gamedata, {flag_red, {set, {key3, value3}}}},
              {gamedata, {flag_red, {set, {key4, value4}}}},
              {gamedata, {set, {limit_time, 5682}}},
              {player, {id_update, {{player_id, iworld1},
                                    {barrier, {set, {is_active, false}}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {barrier, {timer, {set, {key, newvalue}}}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {command, {move, [{posn,127,236}, undefined]}}}}},
              {player, {id_update, {{player_id, iworld1},
                                    {set, {barrier, new_barrier}}}}},
              {player, {id_update, {{player_id, iworld2},
                                    {set, {perk, stealth}}}}},
              {player, {id_update, {{player_id, iworld2},
                                    {set, {player_name, new_player}}}}}],
  Collapsed = {many, [{gamedata, {many, [{flag_blue, {many, [{set, {key1, value1}},
                                                             {set, {key2, value2}}]}},
                                         {flag_red,  {many, [{set, {key3, value3}},
                                                             {set, {key4, value4}}]}},
                                         {set, {limit_time, 5682}}]}},
                      {player, {many, [{id_update, {{player_id, iworld1},
                                                    {many, [{barrier, {many, [{set, {is_active, false}},
                                                                              {timer, {set, {key, newvalue}}}]}},
                                                            {command, {move, [{posn,127,236}, undefined]}},
                                                            {set, {barrier, new_barrier}}]}}},
                                       {id_update, {{player_id, iworld2},
                                                    {many, [{set, {perk, stealth}},
                                                            {set, {player_name, new_player}}]}}}]}}]},
%%   ?debugFmt("\n\n~p\n\n",[?COLLAPSE(Expanded),]),
  ?assertEqual(Collapsed, ?COLLAPSE(Expanded)).

collapse_expand_test() ->
  Collapsed = {many, [{gamedata, {many, [{flag_blue, {many, [{set, {key1, value1}},
                                                             {set, {key2, value2}}]}},
                                         {flag_red,  {many, [{set, {key3, value3}},
                                                             {set, {key4, value4}}]}},
                                         {set, {limit_time, 5682}}]}},
                      {player, {many, [{id_update, {{player_id, iworld1},
                                                    {many, [{barrier, {many, [{set, {is_active, false}},
                                                                              {timer, {set, {key, newvalue}}}]}},
                                                            {command, {move, [{posn,127,236}, undefined]}},
                                                            {set, {barrier, new_barrier}}]}}},
                                       {id_update, {{player_id, iworld2},
                                                    {many, [{set, {perk, stealth}},
                                                            {set, {player_name, new_player}}]}}}]}}]},
  ?assertEqual(Collapsed, ?COLLAPSE(?EXPAND(Collapsed))).

player_conversion_test() ->
  Updates = {many, [{#universe.players, {id_update, {{#player.player_id, iworld4},
                                                     {set, {#player.player_position, posn1}}}}},
                    {#universe.gamedata, {set, {#gamedata.limit_capture, 1}}},
                    {#universe.players, {id_update, {{#player.player_id, iworld5},
                                                     {many, [{#player.barrier, {set, {#player.player_position, pos2}}},
                                                             {set, {#player.is_invisible, false}},
                                                             {set, {#player.is_radar, false}}]}}}},
                    {#universe.players, {id_update, {{#player.player_id, iworld4},
                                                     {command, {command_move, posn3}}}}},
                    {#universe.players, {id_update, {{#player.player_id, iworld6},
                                                     {command, {command_move, posn4}}}}}]},

  ?assertEqual({many, [{#world.gamedata, {set, {#gamedata.limit_capture, 1}}},
                       {#world.otherplayers, {many, [{id_update, {{#player.player_id, iworld5},
                                                                  {many, [{#player.barrier, {set, {#player.player_position, pos2}}},
                                                                          {set, {#player.is_invisible, false}}]}}},
                                                     {id_update, {{#player.player_id, iworld6},
                                                                  {command, {command_move, posn4}}}}]}},
                       {#world.player, {many, [{command, {command_move, posn3}},
                                               {set, {#player.player_position, posn1}}]}}]},
               messaging:convert_universe_update(Updates, iworld4)),

  ?assertEqual({many, [{#world.gamedata, {set, {#gamedata.limit_capture, 1}}},
                       {#world.otherplayers, {many, [{id_update, {{#player.player_id, iworld4},
                                                                  {many, [{command, {command_move, posn3}},
                                                                          {set, {#player.player_position, posn1}}]}}},
                                                     {id_update, {{#player.player_id, iworld6},
                                                                  {command, {command_move, posn4}}}}]}},
                       {#world.player, {many, [{#player.barrier, {set, {#player.player_position, pos2}}},
                                               {set, {#player.is_invisible, false}},
                                               {set, {#player.is_radar, false}}]}}]},
               messaging:convert_universe_update(Updates, iworld5)),

  ?assertEqual({many, [{#world.gamedata, {set, {#gamedata.limit_capture, 1}}},
                       {#world.otherplayers, {many, [{id_update, {{#player.player_id, iworld4},
                                                                  {many, [{command, {command_move, posn3}},
                                                                          {set, {#player.player_position, posn1}}]}}},
                                                     {id_update, {{#player.player_id, iworld5},
                                                                  {many, [{#player.barrier, {set, {#player.player_position, pos2}}},
                                                                          {set, {#player.is_invisible, false}}]}}}]}},
                       {#world.player, {command, {command_move, posn4}}}]},
               messaging:convert_universe_update(Updates, iworld6)).





