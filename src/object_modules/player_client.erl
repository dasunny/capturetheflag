-module(player_client).

-include("capturetheflag.hrl").
-include("barrier.hrl").
-include("timer.hrl").

-export([new/0, tick/2]). %World functions
-export([activate_perk/1, can_sprint/1,
         switch_sprint_on/1, switch_sprint_off/1, update_move_direction/2]). %Utility functions

%% ====================================================================
%% World Functions
%% ====================================================================

new() ->
  player:new(?PLAYER_TYPE_CLIENT).

tick(Player, Barriers) ->
  #player{animation   = Animation,
          timer_reach = Reach} = Player,
  {MoveUpdate, ServerMessage} = move(Player, Barriers),
  PlayerUpdate = {many, [MoveUpdate,
                         {#player.animation, animation:tick(Animation)},
                         {#player.timer_reach, ctf_timer:tick(Reach)}]},
  {PlayerUpdate, ServerMessage}.

%% ====================================================================
%% Utility Functions
%% ====================================================================

activate_perk(#player{player_perk = ?PERK_RADAR}   = Player) -> switch_radar(Player);
activate_perk(#player{player_perk = ?PERK_STEALTH} = Player) -> switch_stealth(Player);
activate_perk(#player{player_perk = ?PERK_BARRIER} = Player) -> call_barrier(Player);
activate_perk(_Player)                                       -> undefined.

can_sprint(#player{timer_exhaustion = ExhTimer}) -> ?TIMER_ISEXPIRED(ExhTimer).

switch_sprint_on(#player{timer_exhaustion = Timer}) when not ?TIMER_ISEXPIRED(Timer) ->
  undefined;
switch_sprint_on(_Player) ->
  {set, {#player.is_sprinting, true}}.

switch_sprint_off(_Player) ->
  {set, {#player.is_sprinting, false}}.

update_move_direction(_Player, Direction) ->
  {set, {#player.move_direction, Direction}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

switch_radar(#player{is_radar = true}) ->
  deactivate;
switch_radar(#player{is_radar = false}) ->
  activate.

switch_stealth(#player{is_invisible = true}) ->
  deactivate;
switch_stealth(#player{timer_invisibility = Timer,
                       has_flag           = false}) ->
  case ?TIMER_GETVALUE(Timer) >= ?GETGAMERULE(#gamerule.invis_min_health) of
    true  -> activate;
    false -> undefined
  end;
switch_stealth(_Player) ->
  undefined.

call_barrier(#player{look_angle      = Look,
                     player_position = PlayerPosition}) ->
  {Displacement, Orientation} =
    case ((Look+45) div 90) rem 4 of
      0 -> {?Posn( 15,   0), ?BARRIER_ORIENTATION_NS};
      1 -> {?Posn(  0, -15), ?BARRIER_ORIENTATION_EW};
      2 -> {?Posn(-15,   0), ?BARRIER_ORIENTATION_NS};
      3 -> {?Posn(  0,  15), ?BARRIER_ORIENTATION_EW}
        end,
  {call, ?Translate(PlayerPosition, Displacement), Orientation}.

move(#player{move_direction = undefined}, _Barriers) ->
  {undefined, undefined};
move(#player{is_jailed = true}, _Barriers) ->
  {undefined, undefined};
move(#player{player_position = Position} = Player, Barriers) ->
  %% Filter out the barriers that are not reachable from my current position
  MaxPossibleTravelDistance = ?GETGAMERULE(#gamerule.speed_sprint)*3,
  NearBarriers = [Barrier || Barrier <- Barriers, barrier:is_near(Barrier, Position, MaxPossibleTravelDistance+1)],
  Displacement = move_distance(Player),
  {Dx, Dy} = case is_stuck(Position, NearBarriers) of
               true  -> {Ax, Ay} = Displacement,
                        case is_stuck(?Translate(Position, Ax, Ay), NearBarriers) of
                          true  -> {0, 0};
                          false -> Displacement
                        end;
               false -> barrier:collision_detection(Position, Displacement, NearBarriers)
             end,
  case {Dx, Dy} of
    {0, 0} -> {undefined, undefined};
    _      -> NewPosition = posn:displace(Position, Dx, Dy),
              {{many, [{set, {#player.player_position, NewPosition}},
                       {#player.animation, {command, reset_walk}}]}, {move, NewPosition}}
  end.

move_direction_to_multipliers(up)        -> {1,   1, false};
move_direction_to_multipliers(down)      -> {-1, -1, false};
move_direction_to_multipliers(left)      -> {-1,  1, true};
move_direction_to_multipliers(right)     -> {1,  -1, true};
move_direction_to_multipliers(undefined) -> {0,   0, false}.

move_distance(#player{is_sprinting     = Sprinting,
                      mouse_difference = MouseDifference,
                      move_direction   = Direction,
                      look_angle       = LookAngle}) ->
  ?Posn(Dx, Dy) = MouseDifference,
  {X, Y, Orbit} = move_direction_to_multipliers(Direction),
  DX = get_displacement(Dx, Sprinting)*utilities:unit(Dx),
  DY = get_displacement(Dy, Sprinting)*utilities:unit(Dy),
  FDX = X*DX*get_angle_multiplier(LookAngle),
  FDY = Y*DY*get_angle_multiplier(LookAngle+90),
  ?IFTHENELSE(Orbit,
              {FDY, FDX},
              {FDX, FDY}).

get_displacement(D, _Sprinting) when ?ISBETWEEN(-5, D, 5) ->
  0;
get_displacement(_D, true) -> ?GETGAMERULE(#gamerule.speed_sprint);
get_displacement(D, _Sprinting) ->
  Div = 25,
  min(?GETGAMERULE(#gamerule.speed_walk), abs(D) div Div + 1).

get_angle_multiplier(Angle) ->
  A2 = Angle rem 180,
  (abs(A2-90) + 15) div 30.

is_stuck(Position, Barriers) ->
  lists:any(fun(Barrier) -> barrier:is_within(Barrier, Position) end, Barriers).

%% check_for_obstacles(_Position, {Dx, Dy}, []) -> {Dx, Dy};
%% check_for_obstacles(_Position, {0, 0},    _) -> {0, 0};
%% check_for_obstacles(Position,  {Dx, Dy}, Barriers) ->
%%   Unit = fun utilities:unit/1,
%%   %% Create the list of steps possible in each direction
%%   XSeq = lists:seq(0, Dx, Unit(Dx)),
%%   YSeq = lists:seq(0, Dy, Unit(Dy)),
%%   Fun1 = fun(Barrier, Displacement) -> not barrier:is_within(Barrier, ?Translate(Position, Displacement)) end,
%%   %% Recurse over the barriers, if any of the points in the step intersect with a barrier, drop the point from the
%%   %% list, and pass the new list to the next recursion. The result will be a list of possible steps that don't
%%   %% intersect with any barriers.
%%   Fun2 = fun(Barrier, {TDx, TDy}) -> MaxDx = lists:takewhile(fun(D) -> Fun1(Barrier, ?Posn(D, 0)) end, TDx),
%%                                      MaxDy = lists:takewhile(fun(D) -> Fun1(Barrier, ?Posn(0, D)) end, TDy),
%%                                      {MaxDx, MaxDy} end,
%%   Seqs = lists:foldl(Fun2, {XSeq, YSeq}, Barriers),
%%   %% Take the last value of the step list and use it as the max displacement. If all of the steps were dropped, use 0
%%   %% as no displacement is possible in that direction.
%%   case Seqs of
%%     {[], []} -> {0,              0};
%%     {[], Ys} -> {0,              lists:last(Ys)};
%%     {Xs, []} -> {lists:last(Xs), 0};
%%     {Xs, Ys} -> {lists:last(Xs), lists:last(Ys)}
%%   end.
