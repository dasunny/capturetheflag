-module(universe).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([diagnostic/1, leave/2, message/3, new/0, new/2, tick/1]).

new() ->
  MapList = map:get_list(),
  GameData = ?UPDATE(gamedata:new(), {set, {#gamedata.map_list, MapList}}),
  #universe{gamedata = GameData,
            players  = [],
            phase    = ?PHASE_INGAME}.

diagnostic(Universe) ->
  [{"# Players", integer_to_list(length(Universe#universe.players))},
   {"Blue Flag", io_lib:format("~p", [Universe#universe.gamedata#gamedata.flag_blue])},
   {"Red Flag", io_lib:format("~p", [Universe#universe.gamedata#gamedata.flag_red])},
   {"Players"}] ++ [{io_lib:format("~p", [Player])} || Player <- Universe#universe.players].

leave(Universe, IWorld) ->
  leave_internal(Universe, IWorld).

message(Universe, IWorld, Message) ->
  message_internal(Universe, IWorld, Message).

new(Universe, IWorld) ->
  new_internal(Universe, IWorld).

tick(#universe{players = []} = Universe) ->
  ?MakeBundle(Universe);
tick(Universe) ->
  tick_internal(Universe).

%% ====================================================================
%% Internal functions
%% ====================================================================

new_internal(Universe, IWorld) ->
  Player = player_server:new(IWorld),
  Update = {add_to_list, {#universe.players, Player}},
  ?MakeBundle(?UPDATE(Universe, Update),
              [make_connect_mail(Universe, IWorld)]).

leave_internal(#universe{players = Players} = Universe, IWorld) ->
  NewPlayerList = player:get_other_players(Players, IWorld),
  #player{player_position = Position,
          player_team     = Team,
          has_flag        = HasFlag} = player:get_player(Players, IWorld),
  Update = case {Team, HasFlag} of
             {_,    false} -> undefined;
             {blue,     _} -> {#world.gamedata, {#gamedata.flag_red,  {command, {drop, [Position]}}}};
             {red,      _} -> {#world.gamedata, {#gamedata.flag_blue, {command, {drop, [Position]}}}}
           end,
  NewUniverse = ?UPDATE(Universe#universe{players = NewPlayerList}, Update),
  Mails = messaging:make_mails(Players, {many, [{#world.otherplayers, {id_delete, {#player.player_id, IWorld}}},
                                                Update]}),
  ?MakeBundle(NewUniverse, Mails).

make_connect_mail(#universe{gamedata = GameData,
                            players  = Players}, Id) ->
  ?MakeMail(Id, {many, [{#world.player, {set, {#player.player_id, Id}}},
                        gamerule:create_game_rule_update(),
                        {set, {#world.gamedata, gamedata:create_short_gamedata(GameData)}},
                        {#world.gamedata, {command, {load_map, [map:load(GameData#gamedata.loaded_map)]}}},
                        {set, {#world.otherplayers, player:make_otherplayers(Players)}}]}).

%Broadcast-to-source messages
message_internal(#universe{players = Players} = Universe,
                 IWorld,
                 {move, Posn}) ->
  Player = player:get_player(Players, IWorld),
  Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, player_server:move(Player, Posn)}}},
  update_and_broadcast(Universe, [Player], Update);

%Broadcast-to-all messages
message_internal(#universe{gamedata = GameData,
                           players  = Players} = Universe, IWorld, {handshake, Name, Perk, PreferredTeam}) ->
  ActualTeam = player:choose_team(Players, PreferredTeam, GameData),
  NewPlayer = player:get_player(Universe#universe.players, IWorld),
  ConvertedPlayer = player:convert_to_player_other(NewPlayer),
  UpdateSet = {many, [{set, {#player.player_name,     Name}},
                      {set, {#player.player_perk,     Perk}},
                      {set, {#player.player_team,     ActualTeam}},
                      {set, {#player.player_position, utilities:get_next_respawn_position(ActualTeam)}},
                      {set, {#player.phase, ?PHASE_INGAME}},
                      {#player.barrier, {set, {#barrier.team, ActualTeam}}}]},
  UniverseUpdate = {#universe.players, {id_update, {{#player.player_id, IWorld}, UpdateSet}}},
  PlayerUpdate  = {many, [{#world.player, UpdateSet},
                          {command, {switch_phase, [?PHASE_INGAME]}}]},
  OthersUpdate  = {many, [{add_to_list, {#world.otherplayers, ConvertedPlayer}},
                          {#world.otherplayers, {id_update, {{#player.player_id, IWorld}, UpdateSet}}}]},
  NewUniverse = ?UPDATE(Universe, UniverseUpdate),
  ?Put("Player ~p joined the ~p team with perk ~p", [Name, PreferredTeam, Perk]),
  Mails1 = ?MakeMail(IWorld, PlayerUpdate),
  Mails2 = messaging:make_mails_for_all_but(Players, IWorld, OthersUpdate),
  ?MakeBundle(NewUniverse, [Mails1 | Mails2]);
message_internal(#universe{players = Players} = Universe, IWorld, {set_perk, Perk}) ->
  Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, {set, {#player.player_perk, Perk}}}}},
  update_and_broadcast(Universe, Players, Update);

message_internal(#universe{gamedata = GameData,
                           players  = Players} = Universe, IWorld, grab) ->
  Update = player_server:grab(Players, IWorld, GameData),
  update_and_broadcast(Universe, Players, Update);

message_internal(#universe{players = Players} = Universe, IWorld, activate) ->
  Player = player:get_player(Players, IWorld),
  case player_server:can_activate(Player) of
    true  -> Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, {command, activate}}}},
             update_and_broadcast(Universe, Players, Update);
    false -> ?MakeBundle(Universe)
  end;

message_internal(#universe{players = Players} = Universe, IWorld, deactivate) ->
  Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, {command, deactivate}}}},
  update_and_broadcast(Universe, Players, Update);

message_internal(#universe{gamedata = GameData,
                           players  = Players} = Universe, IWorld, {call, Posn, Orientation}) ->
  %% Not using the command framework here because I want the server to do all the work on this calculation.
  %% There's no need for all the clients to do duplicate work for the same thing.
  Player = player:get_player(Players, IWorld),
  FieldBarriers = utilities:get_all_barriers(GameData, Players),
  BarrierUpdate = barrier:call(Player#player.barrier, Posn, Orientation, FieldBarriers),
  Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, {#player.barrier, BarrierUpdate}}}},
  update_and_broadcast(Universe, Players, Update);

message_internal(#universe{players = Players} = Universe, IWorld, {look, Angle}) ->
  Update = {#universe.players, {id_update, {{#player.player_id, IWorld}, {command, {look, [Angle]}}}}},
  update_and_broadcast(Universe, Players, Update);

message_internal(Universe, _IWorld, undefined) ->
  ?MakeBundle(Universe);

message_internal(Universe, _IWorld, Message) ->
  io:format("Unrecognized message~n~p~n", [Message]),
  ?MakeBundle(Universe).



update_and_broadcast(Universe, Players, Update) ->
  Mails = messaging:send(Players, Update),
  ?MakeBundle(?UPDATE(Universe, Update), Mails).

tick_internal(#universe{players  = Players,
                        gamedata = GameData} = Universe) ->
  PlayersUpdate  = {#universe.players, {many, [player:tick(Player, undefined) || Player <- Players]}},
  GameDataUpdate = {#universe.gamedata, gamedata:tick(GameData)},
  Update = {many, [PlayersUpdate,
                   GameDataUpdate]},
  Mails = messaging:send(Players, Update),
%%   io:format("Update:\n~p\nMails:\n~p\n",[?CLEANUP(Update),Mails]),%%TODO
  ?MakeBundle(?UPDATE(Universe, Update), Mails).
