-module(world).

-include("capturetheflag.hrl").

-export([draw/3, key/3, mouse/4, new/0, tick/1, diagnostic/1]). %World functions
-export([switch_phase/2]). %Command functions

%% ====================================================================
%% World Functions
%% ====================================================================

diagnostic(World) ->
  [{"Player", io_lib:format("~p",    [World#world.player])},
   {"OPlayers", io_lib:format("~p",  [World#world.otherplayers])},
   {"Blue Flag", io_lib:format("~p", [World#world.gamedata#gamedata.flag_blue])},
   {"Red Flag", io_lib:format("~p",  [World#world.gamedata#gamedata.flag_red])}].

draw(World, Canvas, RenderOpts) ->
  draw_internal(World, Canvas, RenderOpts).

key(World, Key, Type) ->
  create_response(World, key_internal(World, Key, Type)).

mouse(World, X, Y, Type) ->
  create_response(World, mouse_internal(World, X, Y, Type)).

new() ->
  #world{player       = player:new(?PLAYER_TYPE_CLIENT),
         otherplayers = [],
         mouse        = mouse:new(),
         phase        = ?PHASE_CHOOSING,
         gamedata     = gamedata:new(),
         messages     = [],
         arena_size   = {1000, 500}}.

tick(World) ->
  create_response(World, tick_internal(World)).

%% ====================================================================
%% Command Functions
%% ====================================================================

switch_phase(_World, Phase) ->
  panes:switch_view(Phase),
  {set, {#world.phase, Phase}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

create_response(World, {Update, ServerMessage}) ->
  NewWorld = ?UPDATE(World, ?CLEANUP(Update)),
  case ServerMessage of
    undefined -> NewWorld;
    _         -> ?MakePackage(NewWorld, ServerMessage)
  end.

get_barriers(#world{player = #player{player_perk = ?PERK_VAULT,
                                     player_team = Team},
                    gamedata = GameData}) ->
  gamedata:get_non_vaultable_field_barriers(GameData, Team);
get_barriers(#world{player = #player{barrier = Barrier,
                                     player_team = Team},
                    otherplayers = OtherPlayers,
                    gamedata = GameData}) ->
  OPBarriers = [Barrier || #player{barrier = #barrier{is_active = true,
                                                      position = Position} = Barrier} <- OtherPlayers,
                           Position /= undefined],
  FieldBarriers = gamedata:get_all_field_barriers(GameData, Team),
  [Barrier] ++ OPBarriers ++ FieldBarriers.

draw_internal(#world{player       = Player,
                     otherplayers = OtherPlayers,
                     gamedata     = GameData},
              Canvas, RenderOpts) ->
  Fun = fun(OtherPlayer, AccCanvas) -> player:draw(OtherPlayer, AccCanvas, RenderOpts) end,
  C1 = gamedata:draw(GameData, Canvas, RenderOpts),
  C2 = lists:foldl(Fun, C1, OtherPlayers),
  C3 = player:draw(Player, C2, RenderOpts),
  C3.

key_internal(#world{player = Player}, Key, Type)
  when Key == "w"; Key == "s"; Key == "a" ; Key == "d" ->
  Direction =
    case {Key, Type} of
      {"w", key_down} -> up;
      {"s", key_down} -> down;
      {"a", key_down} -> left;
      {"d", key_down} -> right;
      {_, key_up}     -> undefined
    end,
  {{#world.player, player_client:update_move_direction(Player, Direction)}, undefined};

key_internal(#world{player = Player}, shift, key_down) ->
  {{#world.player, player_client:switch_sprint_on(Player)}, undefined};
key_internal(#world{player = Player}, shift, key_up)   ->
  {{#world.player, player_client:switch_sprint_off(Player)}, undefined};

key_internal(#world{player = Player}, " ", key_down) ->
  Command = case player:can_grab(Player) of
              true  -> grab;
              false -> undefined
            end,
  {undefined, Command};

key_internal(#world{player = Player}, "q", key_down) ->
  Action = player_client:activate_perk(Player),
  {undefined, Action};

key_internal(_World, Key, key_down)
  when Key == "1"; Key == "2"; Key == "3" ; Key == "4"; Key == "5" ; Key == "6" ->
  Perk = case Key of
           "1" -> ?PERK_SPRINT;
           "2" -> ?PERK_BARRIER;
           "3" -> ?PERK_STEALTH;
           "4" -> ?PERK_RADAR;
           "5" -> ?PERK_VAULT;
           "6" -> ?PERK_PAROLE
         end,
  {undefined, {set_perk, Perk}};

%% key_internal(World, escape, key_down) ->
%%   interactive_erlang:quit(World);

key_internal(_World, _, _) ->
  {undefined, undefined}.

mouse_internal(World, X, Y, Type) ->
  ?Posn(Dx, Dy) = panes:calculate_scroll_displacement(World#world.arena_size,
                                                      World#world.gamedata#gamedata.map_data#map_data.size,
                                                      World#world.player#player.player_position,
                                                      true),
  NewMouse = mouse:update(World#world.mouse, X - Dx, Y - Dy, Type),
  {NewAngle, NewDistance} = mouse:get_mouse_vector(NewMouse, World#world.player),
  Update = {many, [{set, {#world.mouse, NewMouse}},
                   {#world.player, {many, [{set, {#player.look_angle, NewAngle}},
                                           {set, {#player.mouse_difference, NewDistance}}]}}]},
  case NewAngle == World#world.player#player.look_angle of
    true  -> {Update, undefined};
    false -> {Update, {look, NewAngle}}
  end.

tick_internal(#world{otherplayers = OtherPlayers,
                     player       = Player,
                     messages     = Messages} = World) ->
  {PlayerUpdate, ServerMessage} = player:tick(Player, get_barriers(World)),
  MessageUpdate = message:tick(Messages),
  OtherPlayerUpdates = {many, [player:tick(OtherPlayer, undefined) || OtherPlayer <- OtherPlayers]},
  WorldUpdate = {many, [{#world.otherplayers, OtherPlayerUpdates},
                        {#world.player, PlayerUpdate},
                        {#world.messages, MessageUpdate}]},
  {WorldUpdate, ServerMessage}.
