-module(gamedata).

-include("capturetheflag.hrl").
-include("barrier.hrl").

-export([draw/3, new/0, tick/1]). %World functions
-export([apply_map/2, create_short_gamedata/1]). %Universe functions
-export([get_all_field_barriers/2, get_non_vaultable_field_barriers/2]). %Utility functions
-export([capture/2, load_map/2]). %Command functions

%% ====================================================================
%% World Functions
%% ====================================================================

draw(GameData, Canvas, RenderOpts) ->
  draw_internal(GameData, Canvas, RenderOpts).

new() ->
  #gamedata{limit_time    = 600 * ?FPS,
            limit_capture = 7,
            map_list      = [],
            score_blue    = 0,
            score_red     = 0,
            flag_blue     = flag:new(blue, ?Posn(-100,-100)),
            flag_red      = flag:new(red,  ?Posn(-100,-100))}.

tick(#gamedata{flag_blue = BlueFlag,
               flag_red  = RedFlag} = GameData) ->
  {many, [{#gamedata.flag_blue, flag:tick(BlueFlag)},
          {#gamedata.flag_red,  flag:tick(RedFlag)},
          tick_internal(GameData)]}.

%% ====================================================================
%% Universe Functions
%% ====================================================================

create_short_gamedata(GameData) ->
  GameData#gamedata{map_list = undefined}.

%% ====================================================================
%% Utility Functions
%% ====================================================================

get_all_field_barriers(#gamedata{map_data = #map_data{field = Field}}, Team) ->
  TeamType = case Team of
               blue -> ?BARRIER_VAULT_BLUE;
               red  -> ?BARRIER_VAULT_RED;
               _    -> ?BARRIER_VAULT_NONE
             end,
  [Barrier || #barrier{vault_type = Type} = Barrier <- Field, Type /= TeamType].

get_non_vaultable_field_barriers(#gamedata{map_data = #map_data{field = Field}}, Team) ->
  TeamType = case Team of
               blue -> ?BARRIER_VAULT_RED;
               red  -> ?BARRIER_VAULT_BLUE;
               _    -> ?BARRIER_VAULT_NONE
             end,
  [Barrier || #barrier{vault_type = Type} = Barrier <- Field, Type == ?BARRIER_VAULT_NONE orelse Type == TeamType].

%% ====================================================================
%% Command Functions
%% ====================================================================

apply_map(_GameData, MapPath) ->
  MapData = map:get_map_data_from_path(MapPath),
  #map_data{home_blue = BlueFlag,
            home_red  = RedFlag} = MapData,
  {many, [{set, {#gamedata.flag_blue,  flag:new(blue, BlueFlag)}},
          {set, {#gamedata.flag_red,   flag:new(red,  RedFlag)}},
          {set, {#gamedata.loaded_map, MapPath}},
          {set, {#gamedata.map_data,   MapData}}]}.

capture(_Gamedata, blue) ->
  {many, [{inc, #gamedata.score_red},
          {#gamedata.flag_blue, {command, reset}}]};
capture(_GameData, red) ->
  {many, [{inc, #gamedata.score_blue},
          {#gamedata.flag_red, {command, reset}}]}.

load_map(_GameData, MapZipBinary) ->
  MapData = map:save_map_to_client(MapZipBinary),
  #map_data{name = Name} = MapData,
  ?PreLoadImage(map:file_location(Name, "arena")),
  ?PreLoadImage(map:file_location(Name, "radar")),
  ?PreLoadImage(map:file_location(Name, "mini")),
  {set, {#gamedata.map_data, MapData}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

draw_internal(#gamedata{map_data = #map_data{field = Field}, flag_blue = BlueFlag, flag_red = RedFlag},
              Canvas, RenderOpts) ->
  Fun = fun(#barrier{} = Barrier, AccCanvas) -> barrier:draw(Barrier, AccCanvas, RenderOpts);
           (_Other, AccCanvas) -> AccCanvas end,
  C0 = lists:foldl(Fun, Canvas, Field),
  C1 = flag:draw(BlueFlag, C0, RenderOpts),
  C2 = flag:draw(RedFlag, C1, RenderOpts),
  C2.

tick_internal(#gamedata{limit_time = TimeLimit} = _GameData) ->
  {set, {#gamedata.limit_time, max(0, TimeLimit - 1)}}.
