-module(player_server).

-include("capturetheflag.hrl").
-include("timer.hrl").

-export([new/1, tick/2]). %Universe functions
-export([can_activate/1, grab/3, move/2]). %Utility functions

%% ====================================================================
%% Universe Functions
%% ====================================================================

new(IWorld) ->
  ?UPDATE(player:new(?PLAYER_TYPE_SERVER), {set, {#player.player_id, IWorld}}).

tick(#player{player_id          = Id,
             player_position    = Position,
             barrier            = Barrier,
             is_jailed          = Jailed,
             timer_exhaustion   = ExhTimer,
             timer_jail         = JailTimer,
             timer_invisibility = InvisTimer,
             timer_reach        = ReachTimer,
             timer_sprint       = SprintTimer}, _Userdata) ->
  UnjailMe = case Jailed andalso ?TIMER_ISEXPIRED(JailTimer) of
               true  -> {command, unjail_me};
               false -> undefined
             end,
  ExhaustMe = case ?TIMER_GETVALUE(SprintTimer) =< 0 of
                true  -> {#player.timer_exhaustion, {command, reset}};
                false -> undefined
              end,
  InvisUpdate = case ?TIMER_GETVALUE(InvisTimer) =< 0 of
                  true  -> {many, [{set, {#player.is_invisible, false}},
                                   {#player.timer_invisibility, {set, {#ctf_timer.is_countdown, false}}}]};
                  false -> undefined
                end,
  PlayerUpdate = {many, [{command, {update_position, [Position]}},
                         {#player.barrier,            barrier:tick(Barrier)},
                         {#player.timer_exhaustion,   ctf_timer:tick(ExhTimer)},
                         {#player.timer_jail,         ctf_timer:tick(JailTimer)},
                         InvisUpdate,
                         {#player.timer_invisibility, ctf_timer:tick(InvisTimer)},
                         {#player.timer_reach,        ctf_timer:tick(ReachTimer)},
                         {#player.timer_sprint,       ctf_timer:tick(SprintTimer)},
                         UnjailMe,
                         ExhaustMe]},
  {id_update, {{#player.player_id, Id}, PlayerUpdate}}.

%% ====================================================================
%% Utility Functions
%% ====================================================================

can_activate(#player{player_perk = ?PERK_STEALTH,
                     timer_invisibility = Timer}) -> ?TIMER_GETVALUE(Timer) >= ?GETGAMERULE(#gamerule.invis_min_health);
can_activate(#player{player_perk = ?PERK_RADAR}) -> true;
can_activate(_Player) -> false.

grab(Players, IWorld, GameData) ->
  Player = player:get_player(Players, IWorld),
  case player:can_grab(Player) of
    true  -> OtherPlayers = player:get_other_players(Players, IWorld),
             grab_internal(Player, OtherPlayers, GameData);
    false -> undefined
  end.

move(#player{player_position = OldPosition} = Player, NewPosition) ->
  TotalDeplete = calculate_sprint_depletion(OldPosition, NewPosition),
  {many, [{command, {update_position, [NewPosition]}},
          deplete_sprint(Player, TotalDeplete)]};
move(_Player, _Position) -> undefined.

%% ====================================================================
%% Internal functions
%% ====================================================================

grab_internal(#player{has_flag        = HasFlag,
                      player_id       = MyId,
                      player_position = MyPos,
                      player_team     = MyTeam} = Player, OtherPlayers, GameData) ->
  IsHome = player_is_home(Player, GameData),
  Grab = fun (#player{player_team = TheirTeam} = OtherPlayer)
               when MyTeam == TheirTeam -> sameteam_grab(OtherPlayer, IsHome, MyId);
            (OtherPlayer) -> oppteam_grab(OtherPlayer, IsHome, MyId) end,
  Updates = case HasFlag of
              true  -> []; %% Players can't grab other players while holding the flag
              false -> [Grab(OtherPlayer) || #player{player_position = TheirPos} = OtherPlayer <- OtherPlayers,
                                             posn:is_near_rad(MyPos, TheirPos, ?GETGAMERULE(#gamerule.reach))]
            end,
  GameDataUpdates = gamedata_grab(Player, GameData),
  {many, lists:flatten([GameDataUpdates, {#universe.players, {id_update, {{#player.player_id, MyId},
                                                                          {command, grab}}}} | Updates])}.

sameteam_grab(#player{player_id = TheirId,
                      is_jailed = true}, false, MyId) ->
  [{#universe.players, {id_update, {{#player.player_id, TheirId}, {command, unjail_me}}}},
   {#universe.players, {id_update, {{#player.player_id, MyId}, {#player.player_score, {inc, #score.same_saves}}}}}];
sameteam_grab(_Player, _IsHome, _MyId) ->
  [].

oppteam_grab(#player{is_jailed = true}, true, _MyId) ->
  [];
oppteam_grab(#player{player_id       = TheirId,
                     player_position = Position,
                     is_jailed       = false,
                     has_flag        = true,
                     player_team     = Team}, IsHome, MyId) ->
  Modifier = case IsHome of
               true  -> {command, {jail_me, [utilities:get_next_respawn_position(Team)]}};
               false -> {set, {#player.has_flag, false}}
             end,
  [{#universe.players, {id_update, {{#player.player_id, TheirId}, Modifier}}},
   {#universe.players, {id_update, {{#player.player_id, MyId}, {#player.player_score, {inc, #score.opp_saves}}}}},
   {#universe.gamedata, {utilities:get_other_flag(Team), {command, {drop, [Position]}}}}];


oppteam_grab(#player{player_id = TheirId,
                     player_team = Team,
                     is_jailed = false}, true, MyId) ->
  [{#universe.players, {id_update, {{#player.player_id, TheirId}, {command, {jail_me, [utilities:get_next_respawn_position(Team)]}}}}},
   {#universe.players, {id_update, {{#player.player_id, MyId}, {#player.player_score, {inc, #score.tags}}}}}];
oppteam_grab(_Player, false, _MyId) ->
  [].

gamedata_grab(#player{player_id   = TheirId,
                      player_name = Name,
                      player_team = Team,
                      has_flag    = true} = Player,
              #gamedata{flag_blue = BlueFlag,
                        flag_red  = RedFlag}) ->
  {MyFlag, TheirFlag} =
    case Team of
      blue -> {BlueFlag, RedFlag};
      red  -> {RedFlag, BlueFlag}
    end,
  OtherTeam = utilities:get_other_team(Team),
  case flag:can_capture(TheirFlag, MyFlag, Player) of
    true  -> ?Put("~p captured the ~p flag!", [Name, OtherTeam]),
             [{#universe.players, {id_update, {{#player.player_id, TheirId},
                                               {many, [{set, {#player.has_flag, false}},
                                                       {#player.player_score, {inc, #score.captures}}]}}}},
              {#universe.gamedata, {command, {capture, [OtherTeam]}}}];
    false -> []
  end;
gamedata_grab(#player{player_id       = MyId,
                      player_position = Position,
                      player_team     = Team},
              #gamedata{flag_blue = BlueFlag,
                        flag_red  = RedFlag}) ->
  {Flag, Color} =
    case Team of
      blue -> {RedFlag,  #gamedata.flag_red};
      red  -> {BlueFlag, #gamedata.flag_blue}
    end,
  case flag:can_grab(Flag, Position) of
    true  -> [{#universe.players, {id_update, {{#player.player_id, MyId},
                                               {many, [{set, {#player.has_flag, true}},
                                                       {set, {#player.is_invisible, false}},
                                                       {#player.player_score, {inc, #score.pre_captures}}]}}}},
              {#universe.gamedata, {Color, {command, {grab, [MyId]}}}}];
    false -> []
  end.

calculate_sprint_depletion(OldPosition, NewPosition) ->
  WalkSpeed = ?GETGAMERULE(#gamerule.speed_walk),
  Fun = fun(AbsD) -> max(AbsD - WalkSpeed*3, 0) end,
  ?Posn(Dx, Dy) = posn:difference(OldPosition, NewPosition),
  AbsDx = abs(Dx),
  AbsDy = abs(Dy),
  Fun(AbsDx) + Fun(AbsDy).

deplete_sprint(#player{player_perk = ?PERK_SPRINT}, _Amount) ->
  undefined;
deplete_sprint(_Player, Amount) ->
  {#player.timer_sprint, {command, {dec, [Amount]}}}.

player_is_home(#player{player_position = Position, player_team = Team}, #gamedata{map_data = #map_data{zones = Zones}}) ->
  MyZones = zone:get_team_zones(Zones, Team),
  lists:any(fun(Zone) -> zone:is_within(Zone, Position) end, MyZones).
