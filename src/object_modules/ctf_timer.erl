-module(ctf_timer).

-include("capturetheflag.hrl").

-export([new/5, tick/1]). % World functions
-export([tick_reset/1, resolve/1]).
-export([count_down/1, count_up/1, dec/2, inc/2, pause/1, reset/1, resume/1]). % Command functions

%% ====================================================================
%% World functions
%% ====================================================================

new(InitialValue, MaxValue, MinValue, Direction, StepValue) ->
  #ctf_timer{value        = InitialValue,
             value_max    = MaxValue,
             value_min    = MinValue,
             is_countdown = Direction == -1,
             step         = StepValue,
             can_expire   = true,
             is_expired   = false,
             is_paused    = false}.

tick(#ctf_timer{is_expired = true}) -> undefined;
tick(#ctf_timer{is_paused = true})  -> undefined;
tick(#ctf_timer{step = 0})          -> undefined;

tick(#ctf_timer{value        = UnresolvedValue,
                value_max    = UnresolvedMax,
                value_min    = UnresolvedMin,
                step         = Step,
                is_countdown = Countdown,
                can_expire   = CanExpire}) ->
  Value = resolve(UnresolvedValue),
  Max = resolve(UnresolvedMax),
  Min = resolve(UnresolvedMin),
  if
    not CanExpire, not Countdown, Value == Max -> undefined;
    not CanExpire,     Countdown, Value == Min -> undefined;
    not CanExpire, not Countdown, Value > Max -> {set, {#ctf_timer.value, Max}};
    not CanExpire,     Countdown, Value < Min -> {set, {#ctf_timer.value, Min}};
    Countdown, Value =< Min -> {many, [{set, {#ctf_timer.is_expired, CanExpire}},
                                       {set, {#ctf_timer.value, Min}}]};
    not Countdown, Value >= Max -> {many, [{set, {#ctf_timer.is_expired, CanExpire}},
                                           {set, {#ctf_timer.value, Max}}]};
    Countdown -> {command, {dec, [Step]}};
    not Countdown -> {command, {inc, [Step]}};
    true -> undefined
  end.

%% ====================================================================
%% Command functions
%% ====================================================================

count_down(_Timer) -> {set, {#ctf_timer.is_countdown, true}}.

count_up(_Timer) -> {set, {#ctf_timer.is_countdown, false}}.

dec(#ctf_timer{value     = UnresolvedValue,
               value_min = UnresolvedMin}, Decrement) ->
  Value = resolve(UnresolvedValue),
  Min = resolve(UnresolvedMin),
  case Value >= Min of
    true  -> {set, {#ctf_timer.value, max(Value - Decrement, Min)}};
    false -> undefined
  end.

inc(#ctf_timer{value     = UnresolvedValue,
               value_max = UnresolvedMax}, Increment) ->
  Value = resolve(UnresolvedValue),
  Max = resolve(UnresolvedMax),
  case Value =< Max of
    true  -> {set, {#ctf_timer.value, min(Value + Increment, Max)}};
    false -> undefined
  end.

pause(_Timer) -> {set, {#ctf_timer.is_paused, true}}.

reset(#ctf_timer{is_countdown = Countdown, value_max = Max, value_min = Min}) ->
  Value = case Countdown of
            true  -> {set, {#ctf_timer.value, resolve(Max)}};
            false -> {set, {#ctf_timer.value, resolve(Min)}}
          end,
  {many, [Value,
          {set, {#ctf_timer.is_expired, false}}]}.

resume(_Timer) -> {set, {#ctf_timer.is_paused, false}}.

%% ====================================================================
%% Utility functions
%% ====================================================================

tick_reset(Timer) ->
  case Timer#ctf_timer.is_expired of
    true  -> reset(Timer);
    false -> tick(Timer)
  end.

resolve({gamerule, GameRule}) -> ?GETGAMERULE(GameRule);
resolve(Value) -> Value.
