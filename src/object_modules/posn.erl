-module(posn).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([new/2, displace/3, dx/2, dy/2, difference/2, update/3,
         is_near_rad/3, is_nearSquare/3, is_nearSquare/4, is_within/4]).

new(X, Y) ->
  ?Posn(X, Y).

displace(Posn, Dx, Dy) ->
  ?Translate(Posn, Dx, Dy).

dx(Posn, Dx) ->
  ?Translate(Posn, Dx, 0).

dy(Posn, Dy) ->
  ?Translate(Posn, 0, Dy).

difference(Posn1, Posn2) ->
  ?Difference(Posn1, Posn2).

update(_Posn, NewX, NewY) ->
  ?Posn(NewX, NewY).

is_near_rad(Posn1, Posn2, Distance) ->
  ?Posn(Dx, Dy) = ?Difference(Posn1, Posn2),
  Distance > trunc(math:sqrt(Dx*Dx+Dy*Dy)) + 1.

is_nearSquare(Posn1, Posn2, Distance) ->
  is_nearSquare(Posn1, Posn2, Distance, Distance).

is_nearSquare(?Posn(X1, Y1), ?Posn(X2, Y2), XDistance, YDistance) ->
  ?ISBETWEEN(X2 - XDistance, X1, X2 + XDistance) andalso
      ?ISBETWEEN(Y2 - YDistance, Y1, Y2 + YDistance).

is_within(?Posn(X1, Y1), ?Posn(X2, Y2), Width, Height) ->
  ?ISBETWEEN(X2, X1, X2 + Width) andalso
      ?ISBETWEEN(Y2, Y1, Y2 + Height).
