-module(player_other).

-include("capturetheflag.hrl").

-export([new/0, tick/2]). %World functions
-export([populate/5]). %Command functions

%% ====================================================================
%% World Functions
%% ====================================================================

new() ->
  player:new(?MODULE).

tick(#player{animation   = Animation,
             player_id   = Id,
             timer_reach = Timer}, _Userdata) ->
  {id_update, {{#player.player_id, Id}, {many, [{#player.animation, animation:tick(Animation)},
                                                {#player.timer_reach, ctf_timer:tick(Timer)}]}}}.

%% ====================================================================
%% Utility Functions
%% ====================================================================


%% ====================================================================
%% Command Functions
%% ====================================================================

populate(_OtherPlayer, Position, Perk, Team, Name) ->
  {many, [{set, {#player.player_position, Position}},
          {set, {#player.player_perk, Perk}},
          {set, {#player.player_team, Team}},
          {set, {#player.player_name, Name}}]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

