-module(mouse).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([new/0, update/4, left_down/1, right_down/1]).
-export([get_mouse_vector/2]). %Other functions

new() ->
  #mouse{position   = posn:new(0, 0),
         left_down  = false,
         right_down = false}.

update(Mouse, _, _, left_down)  -> Mouse#mouse{left_down = true};
update(Mouse, _, _, left_up)    -> Mouse#mouse{left_down = false};
update(Mouse, _, _, right_down) -> Mouse#mouse{right_down = true};
update(Mouse, _, _, right_up)   -> Mouse#mouse{right_down = false};
update(Mouse, X, Y, motion)     -> Mouse#mouse{position = posn:update(Mouse#mouse.position, X, Y)}.

left_down(Mouse) ->
  Mouse#mouse.left_down.

right_down(Mouse) ->
  Mouse#mouse.right_down.

get_mouse_vector(#mouse{position = MousePosition},
               #player{player_position = PlayerPosition}) ->
  ?Posn(Dx, Dy) = Difference = ?Difference(MousePosition, PlayerPosition),
  RawAngle = math:atan2(-Dy, Dx)*180 / math:pi(),
  AdjustedAngle = ((trunc(RawAngle) + 360) rem 360) + 15,
  {((AdjustedAngle div 30) * 30) rem 360, Difference}.

%% ====================================================================
%% Internal functions
%% ====================================================================


