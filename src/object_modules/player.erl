-module(player).

-include("capturetheflag.hrl").
-include("timer.hrl").

-export([draw/3, new/1, tick/2]).
-export([can_grab/1, choose_team/3, strip_private_data/1]).
-export([get_player_id/1, get_player/2, get_other_players/2, make_otherplayers/1, convert_to_player_other/1]).
-export([activate/1, deactivate/1, grab/1, jail_me/2, look/2,
         respawn/2, unjail_me/1, update_position/2]). %Command functions

%% ====================================================================
%% World Functions
%% ====================================================================

draw(#player{phase = ?PHASE_INGAME} = Player, Canvas, RenderOpts) ->
  C0 = barrier:draw(Player#player.barrier, Canvas, RenderOpts),
  C1 = draw_player(Player, C0, RenderOpts),
  C2 = draw_jail(Player, C1, RenderOpts),
  C2;
draw(_Player, Canvas, _RenderOpts) -> Canvas.

new(PlayerType) ->
  #player{animation        = animation:new(),
          barrier          = barrier:new(),
          has_flag         = false,
          is_invisible     = false,
          is_jailed        = false,
          is_radar         = false,
          is_sprinting     = false,
          look_angle       = 0,
          mouse_difference = ?Posn(0,0),
          move_direction   = undefined,
          phase            = ?PHASE_CHOOSING,
          player_id        = undefined,
          player_perk      = undefined,
          player_position  = posn:new(-150, -150),
          player_name      = lists:flatten(io_lib:format("NewPlayer~3..0b", [random:uniform(999)])),
          player_score     = score:new(),
          player_team      = undefined,
          player_type      = PlayerType,
          respawn_position = undefined,
          timer_exhaustion = ctf_timer:new(0, {gamerule, #gamerule.max_exhaustion_time}, 0, -1, 1),
          timer_invisibility = ?UPDATE(ctf_timer:new({gamerule, #gamerule.health_invis},
                                                     {gamerule, #gamerule.invis_max_health}, 0, 1, 1),
                                       {set, {#ctf_timer.can_expire, false}}),
          timer_jail       = ctf_timer:new({gamerule, #gamerule.time_jail},
                                           {gamerule, #gamerule.time_jail}, 0, -1, 1),
          timer_reach      = ctf_timer:new(0, {gamerule, #gamerule.reach}, 0, -1, 4),
          timer_sprint     = ?UPDATE(ctf_timer:new({gamerule, #gamerule.health_sprint},
                                                   {gamerule, #gamerule.health_sprint}, 0, 1, 3),
                                     {set, {#ctf_timer.can_expire, false}})}.

tick(#player{player_type = Type} = Player, Userdata) ->
  Type:tick(Player, Userdata).

%% ====================================================================
%% Utility Functions
%% ====================================================================

get_player_id(#player{player_id = ID}) -> ID.

get_player(Players, IWorld) ->
  case lists:keyfind(IWorld, #player.player_id, Players) of
    false  -> undefined;
    Player -> Player
  end.

get_other_players(ListOfPlayers, IWorld) ->
  lists:keydelete(IWorld, #player.player_id, ListOfPlayers).

make_otherplayers(Players) ->
  [convert_to_player_other(Player) || Player <- Players].

-define(IS_PRIVATE(Key), Key == #player.is_radar
                  orelse Key == #player.is_sprinting
                  orelse Key == #player.mouse_difference
                  orelse Key == #player.move_direction
                  orelse Key == #player.timer_exhaustion
                  orelse Key == #player.timer_invisibility
                  orelse Key == #player.timer_sprint).

convert_to_player_other(Player) ->
  ?UPDATE(Player, {many, [{unset, #player.is_radar},
                          {unset, #player.is_sprinting},
                          {unset, #player.mouse_difference},
                          {unset, #player.move_direction},
                          {unset, #player.timer_exhaustion},
                          {unset, #player.timer_invisibility},
                          {unset, #player.timer_sprint},
                          {set,  {#player.player_type, ?PLAYER_TYPE_OTHER}}]}).

%% ====================================================================
%% Called Functions
%% ====================================================================

can_grab(#player{is_invisible = IsInvisible,
                 is_jailed    = IsJailed,
                 timer_reach  = Timer}) ->
  (not IsInvisible) andalso
   (not IsJailed) andalso
   ?TIMER_ISEXPIRED(Timer).

choose_team(_Players, blue, _GameData) -> blue;
choose_team(_Players, red, _GameData) ->  red;
choose_team(Players, auto, GameData) ->
  Fun = fun(#player{player_team = blue}, {B,R}) -> {B+1, R};
           (#player{player_team = red}, {B,R}) ->  {B, R+1};
           (_, Acc) -> Acc end,
  Fun2 = fun(#gamedata{score_blue = BS, score_red = RS}) when BS < RS -> blue;
            (#gamedata{score_blue = BS, score_red = RS}) when BS > RS -> red;
            (_) -> blue end,
  {B, R} = lists:foldl(Fun, {0,0}, Players),
  if
    B < R -> blue;
    B > R -> red;
    B == R -> Fun2(GameData)
  end.

%% ====================================================================
%% Command Functions
%% ====================================================================

activate(#player{player_perk = ?PERK_RADAR,
                 player_type = ?PLAYER_TYPE_CLIENT}) ->
  {'not', #player.is_radar};

activate(#player{player_perk = ?PERK_STEALTH,
                 has_flag = false,
                 player_type = ?PLAYER_TYPE_OTHER}) ->
  {set, {#player.is_invisible, true}};
activate(#player{player_perk = ?PERK_STEALTH,
                 timer_invisibility = Timer,
                 has_flag    = false}) ->
  case ?TIMER_GETVALUE(Timer) >= ?GETGAMERULE(#gamerule.invis_min_health) of
    true  -> {many, [{set, {#player.is_invisible, true}},
                     {#player.timer_invisibility, {set, {#ctf_timer.is_countdown, true}}}]};
    false -> undefined
  end;
activate(_Player) -> undefined.

deactivate(#player{player_perk = ?PERK_STEALTH,
                   player_type = ?PLAYER_TYPE_CLIENT}) ->
  {many, [{set, {#player.is_invisible, false}},
          {#player.timer_invisibility, {set, {#ctf_timer.is_countdown, false}}}]};
deactivate(#player{player_perk = ?PERK_STEALTH,
                   player_type = ?PLAYER_TYPE_OTHER}) ->
  {set, {#player.is_invisible, false}};
deactivate(#player{player_perk = ?PERK_RADAR, player_type = ?PLAYER_TYPE_CLIENT}) ->
  {set, {#player.is_radar, false}};
deactivate(_Player) ->
  undefined.

grab(_Player) ->
  {#player.timer_reach, {command, reset}}.

jail_me(#player{player_perk = Perk}, ResPosition) ->
  JailTime = case Perk of
               ?PERK_PAROLE -> 10*?FPS;
               _            -> 20*?FPS
             end,
  {many, [{set, {#player.is_jailed,        true}},
          {set, {#player.has_flag,         false}},
          {set, {#player.is_invisible,     false}},
          {set, {#player.respawn_position, ResPosition}},
          {#player.timer_jail, {many, [{set, {#ctf_timer.value_max, JailTime}},
                                       {command, reset}]}}]}.

look(#player{player_type = ?PLAYER_TYPE_CLIENT}, _Angle) -> undefined;
look(_Player, Angle) -> {set, {#player.look_angle, Angle}}.

respawn(_PlayerBase, Posn) ->
  {set, {#player.player_position, Posn}}.

unjail_me(#player{respawn_position = RespawnPosition}) ->
  {many, [{set, {#player.is_jailed, false}},
          {set, {#player.player_position, RespawnPosition}},
          {unset, #player.respawn_position}]}.

update_position(#player{player_position = OldPosition,
                        player_type     = ?PLAYER_TYPE_OTHER}, NewPosition) ->
  PositionUpdate = {set, {#player.player_position, NewPosition}},
  case position_unchanged(OldPosition, NewPosition) of
    true  -> PositionUpdate;
    false -> {many, [PositionUpdate,
                     {#player.animation, {command, reset_walk}}]}
  end;
update_position(#player{player_type     = ?PLAYER_TYPE_SERVER}, NewPosition) ->
  {set, {#player.player_position, NewPosition}};
update_position(_Player, _Position) -> undefined.

%% ====================================================================
%% Conversion Functions
%% ====================================================================

%% To be used on flat, player updates only.
strip_private_data({Op, {Key, _}})
  when is_atom(Op) andalso ?IS_PRIVATE(Key) -> undefined;
strip_private_data({Op, Key})
  when is_atom(Op) andalso ?IS_PRIVATE(Key) -> undefined;
strip_private_data({Key, _})
  when ?IS_PRIVATE(Key) -> undefined;
strip_private_data(Update) ->
  Update.

%% ====================================================================
%% Internal Functions
%% ====================================================================

position_unchanged(Position, Position)     -> true;
position_unchanged(_Position1, _Position2) -> false.

draw_player(#player{player_position = Position}, Canvas, RenderOpts) when ?RADAR_ON(RenderOpts) ->
  Displace = ?DISPLACE(RenderOpts),
  ?DrawCircle(Canvas, Displace(Position), 10, ?GREEN);
draw_player(#player{is_invisible = true,
                    player_team  = TheirTeam}, Canvas,
            #render_options{client_team = ClientTeam}) when TheirTeam /= ClientTeam ->
  Canvas;
draw_player(#player{is_invisible = IsInvisible,
                    look_angle = LookAngle,
                    player_position = Position,
                    player_name = Name,
                    player_team = Team,
                    player_type = PlayerType} = Player, Canvas, RenderOpts) ->
  Displace = ?DISPLACE(RenderOpts),

  Opacity = case IsInvisible of
              true  -> 0.5;
              false -> 1.0
            end,
  PlayerCanvas = ?CompositeCanvas(50,60),
  PC0 = case get_legs(Player) of
         undefined -> PlayerCanvas;
         Legs      -> ?DrawImage(PlayerCanvas, ?Posn(0, 8), Legs, [{mask, ?WHITE}])
       end,
  PC1 = case get_torso(Player) of
         undefined -> PC0;
         Torso     -> ?DrawImage(PC0, ?Posn(10, 0), Torso, [{mask, ?WHITE}])
       end,
  PC2 = case get_arms(Player) of
         undefined -> PC1;
         Arms      -> ?DrawImage(PC1, ?Posn(2,0), Arms, [{mask, ?WHITE}])
       end,
  C1 = ?DrawComposite(Canvas, Displace(Position), PC2, [{mask, ?WHITE},
                                                        {rotate, LookAngle, ?Posn(18, 19)},
                                                        {opacity, Opacity}]),

  case PlayerType of
    ?PLAYER_TYPE_OTHER -> TeamColor = utilities:get_color(Team),
                          ?DrawText(C1, Displace(?Translate(Position, -(?TextWidth(Name) div 2), -35)),
                                    Name, TeamColor);
    _ -> C1
  end.

draw_jail(#player{is_jailed = true,
                  player_position = Position}, Canvas, RenderOpts) ->
  Path = case ?RADAR_ON(RenderOpts) of
           true  -> "resources/sprites/jail-green.png";
           false -> "resources/sprites/jail.png"
         end,
  Displace = ?DISPLACE(RenderOpts),
  ?DrawImage(Canvas, Displace(?Translate(Position, -22, -22)),
             Path, [{mask, ?WHITE}]);
draw_jail(_Player, Canvas, _RenderOpts) ->
  Canvas.

get_torso(#player{player_team = Team}) ->
  "resources/sprites/avatars/torso/" ++ utilities:team_to_string(Team) ++ ".png".

get_arms(#player{player_team = Team, has_flag = true}) ->
  "resources/sprites/avatars/arm/" ++ utilities:team_to_string(Team) ++ "-flag.png";
get_arms(#player{player_team = Team,
                 timer_reach = Reach}) when not ?TIMER_ISEXPIRED(Reach)->
  TeamString = utilities:team_to_string(Team),
  GrabString = case ?TIMER_GETVALUE(Reach) of
                 N when N >= 10 -> "-grab-full.png";
                 _              -> "-grab-half.png"
               end,
  "resources/sprites/avatars/arm/" ++ TeamString ++ GrabString;
get_arms(#player{animation = #animation{timer_walk_reset = Timer}}) when ?TIMER_ISEXPIRED(Timer) ->
  undefined;
get_arms(#player{animation = #animation{walk_phase = ?ANIM_PHASE_1}}) ->
  "resources/sprites/avatars/arm/walk1.png";
get_arms(#player{animation = #animation{walk_phase = ?ANIM_PHASE_2}}) ->
  "resources/sprites/avatars/arm/walk2.png".

get_legs(#player{animation = #animation{timer_walk_reset = Timer}}) when ?TIMER_ISEXPIRED(Timer) ->
  undefined;
get_legs(#player{animation = #animation{walk_phase = ?ANIM_PHASE_1}}) ->
  "resources/sprites/avatars/leg/walk1.png";
get_legs(#player{animation = #animation{walk_phase = ?ANIM_PHASE_2}}) ->
  "resources/sprites/avatars/leg/walk2.png".

