-module(barrier).

-include("capturetheflag.hrl").
-include("barrier.hrl").
-include("timer.hrl").

-export([draw/3, new/0, tick/1]). %World functions
-export([collision_detection/3, is_within/2, is_near/3]). %Utility functions
-export([new/2, new_immortal/4]). %Other
-export([call/4, reset/1]). %Command functions

%% ====================================================================
%% World Functions
%% ====================================================================

-define(SIDELENGTH,  15).
-define(BARRIER_MAX_LENGTH, 300).

draw(#barrier{do_draw   = Draw,
              is_active = Active}, Canvas, _RenderOpts) when not Active orelse not Draw -> Canvas;
draw(#barrier{position = ?Posn(X, Y),
              timer    = Timer} = Barrier, Canvas, RenderOpts) ->
  Displace = ?DISPLACE(RenderOpts),
  C0 = draw_barrier(Barrier, Canvas, RenderOpts),
  C1 = case ?RADAR_ON(RenderOpts) of
         true  -> utilities:draw_gauge(C0, ?GREEN, ?DARKGREEN, 40, 6,
                                       ?TIMER_GETVALUE(Timer), ?TIMER_GETMAX(Timer), Displace(?Posn(X, Y-10)), false);
         false -> C0
       end,
  C1.

new() ->
  #barrier{dim_height      = ?SIDELENGTH,
           dim_width       = ?SIDELENGTH,
           do_draw         = true,
           is_active       = false,
           is_immortal     = false,
           max_n_w_reach   = 10,
           max_s_e_reach   = 10,
           orientation     = ?BARRIER_ORIENTATION_NE,
           phase           = ?BARRIER_PHASE_STATIC,
           position        = undefined,
           ref             = make_ref(),
           shape           = ?BARRIER_SHAPE_SQUARE,
           team            = undefined,
           timer           = ctf_timer:new({gamerule, #gamerule.health_barrier},
                                           {gamerule, #gamerule.health_barrier}, 0, -1, 1),
           vault_type      = ?BARRIER_VAULT_ALL}.

tick(#barrier{is_immortal = true}) -> undefined;
tick(#barrier{is_active     = true,
              dim_height    = Height,
              dim_width     = Width,
              max_n_w_reach = MaxNW,
              max_s_e_reach = MaxSE,
              phase         = ?BARRIER_PHASE_EXPANDING,
              orientation   = Orientation}) when
  (Orientation == ?BARRIER_ORIENTATION_NS andalso (Height >= MaxSE-MaxNW)) orelse
    (Orientation == ?BARRIER_ORIENTATION_EW andalso (Width >= MaxSE-MaxNW)) ->
  {set, {#barrier.phase, ?BARRIER_PHASE_STATIC}};
tick(#barrier{is_active     = true,
              dim_height    = Height,
              dim_width     = Width,
              max_n_w_reach = MaxNW,
              max_s_e_reach = MaxSE,
              phase         = ?BARRIER_PHASE_EXPANDING,
              position      = ?Posn(X, Y) = Position,
              orientation   = Orientation}) ->
  {Point, Length} = case Orientation of
                      ?BARRIER_ORIENTATION_NS -> {Y, Height};
                      ?BARRIER_ORIENTATION_EW -> {X, Width}
                    end,
  NWStep = max(Point - 10, MaxNW) - Point,
  SEStep = min(Point + Length + 10, MaxSE) - Point - Length,
  Step = -NWStep + SEStep,
  {NewPosition, NewWidth, NewHeight} =
    case Orientation of
      ?BARRIER_ORIENTATION_NS -> {?Translate(Position, 0, NWStep), ?SIDELENGTH, Height+Step};
      ?BARRIER_ORIENTATION_EW -> {?Translate(Position, NWStep, 0), Width+Step, ?SIDELENGTH}
    end,
  {many, [{set, {#barrier.position, NewPosition}},
          {set, {#barrier.dim_height, NewHeight}},
          {set, {#barrier.dim_width, NewWidth}}]};
tick(#barrier{is_active = true, phase = ?BARRIER_PHASE_STATIC, timer = Timer}) ->
  case ?TIMER_ISEXPIRED(Timer) of
    true  -> {command, reset};
    false -> {#barrier.timer, ctf_timer:tick(Timer)}
  end;
tick(_Barrier) -> undefined.

%% ====================================================================
%% Utility Functions
%% ====================================================================

collision_detection(_Position, {Dx, Dy}, []) -> {Dx, Dy};
collision_detection(_Position, {0, 0},    _) -> {0, 0};
collision_detection(Position,  {Dx, Dy}, Barriers) ->
  Unit = fun utilities:unit/1,
  %% Create the list of steps possible in each direction
  XSeq = lists:seq(0, Dx, Unit(Dx)),
  YSeq = lists:seq(0, Dy, Unit(Dy)),
  Fun1 = fun(Barrier, Displacement) -> not is_within(Barrier, ?Translate(Position, Displacement)) end,
  %% Recurse over the barriers, if any of the points in the step intersect with a barrier, drop the point from the
  %% list, and pass the new list to the next recursion. The result will be a list of possible steps that don't
  %% intersect with any barriers.
  Fun2 = fun(Barrier, {TDx, TDy}) -> MaxDx = lists:takewhile(fun(D) -> Fun1(Barrier, ?Posn(D, 0)) end, TDx),
                                     MaxDy = lists:takewhile(fun(D) -> Fun1(Barrier, ?Posn(0, D)) end, TDy),
                                     {MaxDx, MaxDy} end,
  Seqs = lists:foldl(Fun2, {XSeq, YSeq}, Barriers),
  %% Take the last value of the step list and use it as the max displacement. If all of the steps were dropped, use 0
  %% as no displacement is possible in that direction.
  case Seqs of
    {[], []} -> {0,              0};
    {[], Ys} -> {0,              lists:last(Ys)};
    {Xs, []} -> {lists:last(Xs), 0};
    {Xs, Ys} -> {lists:last(Xs), lists:last(Ys)}
  end.

is_near(#barrier{position = undefined}, _Posn, _Buffer) -> false;
is_near(#barrier{position = ?Posn(Bx, By), dim_width = Width, dim_height = Height},
        ?Posn(Px, Py), Buffer) ->
  ?ISBETWEEN(Bx  - Buffer, Px, Bx + Width + Buffer) andalso
    ?ISBETWEEN(By - Buffer, Py, By + Height + Buffer).

is_within(Barrier, Position) ->
  is_within_int(Barrier, Position).

%% ====================================================================
%% Other Functions
%% ====================================================================

new(Width, Height) ->
  ?NEW({many, [{set, {#barrier.dim_height, Height}},
               {set, {#barrier.dim_width, Width}}]}).

new_immortal(Position, Width, Height, VaultType) ->
  ?NEW({many, [{set, {#barrier.dim_height,  Height}},
               {set, {#barrier.dim_width,   Width}},
               {set, {#barrier.do_draw,     false}},
               {set, {#barrier.is_active,   true}},
               {set, {#barrier.is_immortal, true}},
               {set, {#barrier.position,    Position}},
               {set, {#barrier.vault_type,  VaultType}}]}).

%% ====================================================================
%% Command Functions
%% ====================================================================

call(#barrier{is_active = true}, _Position, _Orientation, _FieldBarriers) ->
  undefined;
call(_Barrier, ?Posn(X, Y) = Position, Orientation, FieldBarriers) ->
  NearBarriers = [Barrier || Barrier <- FieldBarriers, is_near(Barrier, Position, (?BARRIER_MAX_LENGTH div 2) +10)],
  FindSpan = fun(Posn, Displacement) ->
                  collision_detection(Posn, Displacement, NearBarriers) end,
  MaxLengthBack = ?BARRIER_MAX_LENGTH div 2,
  MaxLengthFor  = ?BARRIER_MAX_LENGTH - MaxLengthBack,
  Div2 = ?SIDELENGTH div 2,
  {MaxNWSpan, MaxSESpan} = case Orientation of
                             ?BARRIER_ORIENTATION_NS -> Posn1 = ?Translate(Position, -Div2, 0),
                                                        Posn2 = ?Translate(Position, ?SIDELENGTH - Div2, 0),
                                                        {_, MaxNW} = max(FindSpan(Posn1, {0, -MaxLengthBack}),
                                                                         FindSpan(Posn2, {0, -MaxLengthBack})),
                                                        {_, MaxSE} = min(FindSpan(Posn1, {0, MaxLengthFor}),
                                                                         FindSpan(Posn2, {0, MaxLengthFor})),
                                                        {Y+MaxNW, Y+MaxSE};
                             ?BARRIER_ORIENTATION_EW -> Posn1 = ?Translate(Position, 0, -Div2),
                                                        Posn2 = ?Translate(Position, 0, ?SIDELENGTH - Div2),
                                                        {MaxNW, _} = max(FindSpan(Posn1, {-MaxLengthBack, 0}),
                                                                         FindSpan(Posn2, {-MaxLengthBack, 0})),
                                                        {MaxSE, _} = min(FindSpan(Posn1, {MaxLengthFor, 0}),
                                                                         FindSpan(Posn2, {MaxLengthFor, 0})),
                                                        {X+MaxNW, X+MaxSE}
                           end,
  {many, [{set, {#barrier.position, ?Translate(Position, -Div2, -Div2)}},
          {set, {#barrier.is_active, true}},
          {set, {#barrier.max_n_w_reach, MaxNWSpan}},
          {set, {#barrier.max_s_e_reach, MaxSESpan}},
          {set, {#barrier.orientation, Orientation}},
          {set, {#barrier.phase, ?BARRIER_PHASE_EXPANDING}},
          {#barrier.timer, {command, reset}}]}.

reset(_Barrier) ->
  {many, [{unset, #barrier.position},
          {set, {#barrier.is_active, false}},
          {set, {#barrier.dim_height, ?SIDELENGTH}},
          {set, {#barrier.dim_width, ?SIDELENGTH}}]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

draw_barrier(Barrier, Canvas, RenderOpts) when ?RADAR_ON(RenderOpts) ->
  draw_simple(Barrier, Canvas, RenderOpts, ?GREEN);
draw_barrier(Barrier, Canvas, RenderOpts) when not Barrier#barrier.is_immortal ->
  Displace = ?DISPLACE(RenderOpts),
  C0 = draw_background(Barrier, Canvas, Displace),
  C1 = draw_chevrons(Barrier, C0, Displace),
  C1;
draw_barrier(#barrier{vault_type = VaultType} = Barrier, Canvas, RenderOpts) ->
  Color = case VaultType of
            ?BARRIER_VAULT_ALL  -> ?GRAY;
            ?BARRIER_VAULT_BLUE -> ?COLOR(128, 128, 255);
            ?BARRIER_VAULT_RED  -> ?COLOR(255, 128, 128);
            _                   -> ?BLACK
          end,
  draw_simple(Barrier, Canvas, RenderOpts, Color).

draw_background(#barrier{dim_height  = Height,
                         team        = Team,
                         orientation = ?BARRIER_ORIENTATION_NS,
                         position    = Position}, Canvas, Displace) ->
  TeamString = utilities:team_to_string(Team),
  Rotate = {rotate, 90},
  C0 = ?DrawImage(Canvas, Displace(Position),
                  "resources/barrier/end.png", [Rotate]),
  C1 = ?DrawImage(C0, Displace(?Translate(Position, 0, 0)),
                  "resources/barrier/" ++ TeamString ++ "_back.png", [{resize, {Height-2, ?SIDELENGTH}},
                                                                      {rotate, 90, ?Posn(Height div 2,0)}]),
  C2 = ?DrawImage(C1, Displace(?Translate(Position, 0, Height-1)),
                  "resources/barrier/end.png", [Rotate]),
  C2;
draw_background(#barrier{dim_width   = Width,
                         team        = Team,
                         orientation = ?BARRIER_ORIENTATION_EW,
                         position    = Position}, Canvas, Displace) ->
  TeamString = utilities:team_to_string(Team),
  C0 = ?DrawImage(Canvas, Displace(Position),
                  "resources/barrier/end.png"),
  C1 = ?DrawImage(C0, Displace(?Translate(Position, 1, 0)),
                  "resources/barrier/" ++ TeamString ++ "_back.png", [{resize, {Width-2, ?SIDELENGTH}}]),
  C2 = ?DrawImage(C1, Displace(?Translate(Position, Width-1, 0)),
                  "resources/barrier/end.png"),
  C2.

draw_chevrons(#barrier{dim_height  = Height,
                       team        = Team,
                       orientation = ?BARRIER_ORIENTATION_NS,
                       position    = Position} = _Barrier, Canvas, Displace) ->
  TeamString = utilities:team_to_string(Team),
  Path = "resources/barrier/"++ TeamString,
  Div2 = Height div 2,
  Rem = Height rem 2,
  C0 = case Rem of
         0 -> ?DrawImage(Canvas, Displace(?Translate(Position, 4, Div2-4)),
                         Path ++ "_cen.png", [{mask, ?WHITE},
                                              {rotate, 270, ?Posn(0, 6)}]);
         1 -> ?DrawImage(Canvas, Displace(?Translate(Position, 4, Div2-3)),
                         Path ++ "_cen-odd.png", [{mask, ?WHITE}])
       end,
  Seq1 = lists:seq(Div2 + Rem + 2, Height-8, 9),
  Seq2 = [Height-N-1 || N <- Seq1],
  Fun1 = fun(N, Acc) -> ?DrawImage(Acc, Displace(?Translate(Position, 0, N+1)),
                                   Path ++ "_chev.png", [{mask, ?WHITE},
                                                         {rotate, 270, ?Posn(1, 13)}]) end,
  Fun2 = fun(N, Acc) -> ?DrawImage(Acc, Displace(?Translate(Position, 14, N-1)),
                                   Path ++ "_chev.png", [{mask, ?WHITE},
                                                         {rotate, 90, ?Posn(1,13)}]) end,
  C1 = lists:foldl(Fun1, C0, Seq1),
  C2 = lists:foldl(Fun2, C1, Seq2),
  C2;
  draw_chevrons(#barrier{dim_width   = Width,
                       team        = Team,
                       orientation = ?BARRIER_ORIENTATION_EW,
                       position    = Position} = _Barrier, Canvas, Displace) ->
  TeamString = utilities:team_to_string(Team),
  Path = "resources/barrier/"++ TeamString,
  Div2 = Width div 2,
  Rem = Width rem 2,
  C0 = case Rem of
         0 -> ?DrawImage(Canvas, Displace(?Translate(Position, Div2-4, 4)),
                         Path ++ "_cen.png", [{mask, ?WHITE}]);
         1 -> ?DrawImage(Canvas, Displace(?Translate(Position, Div2-3, 4)),
                         Path ++ "_cen-odd.png", [{mask, ?WHITE}])
       end,
  Seq1 = lists:seq(Div2 + Rem + 2, Width-8, 9),
  Seq2 = [Width-N-1 || N <- Seq1],
  Fun1 = fun(N, Acc) -> ?DrawImage(Acc, Displace(?Translate(Position, N, 1)),
                                   Path ++ "_chev.png", [{mask, ?WHITE}]) end,
  Fun2 = fun(N, Acc) -> ?DrawImage(Acc, Displace(?Translate(Position, N-1, 0)),
                                   Path ++ "_chev.png", [{mask, ?WHITE},
                                                         {rotate, 180, ?Posn(1,13)}]) end,
  C1 = lists:foldl(Fun1, C0, Seq1),
  C2 = lists:foldl(Fun2, C1, Seq2),
  C2.

draw_simple(#barrier{dim_width = Width, dim_height = Height, position = Position},
             Canvas, RenderOpts, Color) ->
  Displace = ?DISPLACE(RenderOpts),
  ?DrawRectangle(Canvas, Displace(Position), Width, Height, Color).

is_within_int(#barrier{position = undefined}, _Posn) -> false;
is_within_int(#barrier{shape = ?BARRIER_SHAPE_SQUARE} = Barrier, Posn) ->
  is_near(Barrier, Posn, 0);

is_within_int(#barrier{shape = ?BARRIER_SHAPE_TRIANGLE} = Barrier, Posn) ->
  geometry:is_within_triangle(Posn, get_3_points(Barrier));

is_within_int(#barrier{position = ?Posn(Bx, By) = BarrierPosn, dim_width = Radius,
                   orientation = ?BARRIER_ORIENTATION_NE, shape = ?BARRIER_SHAPE_CIRCLE},
          ?Posn(Px, Py) = PlayerPosn) when Px =< Bx+Radius, Py >= By ->
  posn:is_near_rad(PlayerPosn, ?Translate(BarrierPosn, Radius, 0), Radius);
is_within_int(#barrier{position = ?Posn(Bx, By) = BarrierPosn, dim_width = Radius,
                   orientation = ?BARRIER_ORIENTATION_SE, shape = ?BARRIER_SHAPE_CIRCLE},
          ?Posn(Px, Py) = PlayerPosn) when Px =< Bx+Radius, Py =< By+Radius ->
  posn:is_near_rad(PlayerPosn, ?Translate(BarrierPosn, Radius, Radius), Radius);
is_within_int(#barrier{position = ?Posn(Bx, By) = BarrierPosn, dim_width = Radius,
                   orientation = ?BARRIER_ORIENTATION_SW, shape = ?BARRIER_SHAPE_CIRCLE},
          ?Posn(Px, Py) = PlayerPosn) when Px >= Bx, Py =< By+Radius ->
  posn:is_near_rad(PlayerPosn, ?Translate(BarrierPosn, 0, Radius), Radius);
is_within_int(#barrier{position = ?Posn(Bx, By) = BarrierPosn, dim_width = Radius,
                   orientation = ?BARRIER_ORIENTATION_NW, shape = ?BARRIER_SHAPE_CIRCLE},
          ?Posn(Px, Py) = PlayerPosn) when Px >= Bx, Py >= By ->
  posn:is_near_rad(PlayerPosn, BarrierPosn, Radius);
is_within_int(#barrier{shape = ?BARRIER_SHAPE_CIRCLE},
          _Posn) -> false.

get_3_points(#barrier{position = P1, dim_width = Width, dim_height = Height, orientation = Orientation}) ->
  T = fun(X,Y) -> ?Translate(P1, X,Y) end,
  case Orientation of
    ?BARRIER_ORIENTATION_NE -> {T(    0, 0), T(Width,      0), T(Width, Height)};
    ?BARRIER_ORIENTATION_SE -> {T(Width, 0), T(Width, Height), T(0,     Height)};
    ?BARRIER_ORIENTATION_SW -> {T(    0, 0), T(Width, Height), T(0,     Height)};
    ?BARRIER_ORIENTATION_NW -> {T(    0, 0), T(Width,      0), T(0,     Height)}
  end.
