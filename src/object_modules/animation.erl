-module(animation).

-include("capturetheflag.hrl").
-include("timer.hrl").

-export([new/0, tick/1]).
-export([reset_walk/1]). %% Command Functions

%% ====================================================================
%% World Functions
%% ====================================================================

new() ->
  #animation{timer_walk_phase = ctf_timer:new(2, 2, 0, -1, 1),
             timer_walk_reset = ctf_timer:new(0, 2, 0, -1, 1),
             walk_phase       = ?ANIM_PHASE_1}.

tick(#animation{timer_walk_phase = WalkPhase,
                timer_walk_reset = WalkReset,
                walk_phase       = Phase}) ->
  ResetUpdate = {#animation.timer_walk_reset, ctf_timer:tick(WalkReset)},
  case ?TIMER_ISEXPIRED(WalkPhase) of
    true  -> {many, [{#animation.timer_walk_phase, {command, reset}},
                     ResetUpdate,
                     {set, {#animation.walk_phase, next_phase(Phase)}}]};
    false -> {many, [{#animation.timer_walk_phase, ctf_timer:tick(WalkPhase)},
                     ResetUpdate]}
  end.

%% ====================================================================
%% Command Functions
%% ====================================================================

reset_walk(_Animation) -> {#animation.timer_walk_reset, {command, reset}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

next_phase(?ANIM_PHASE_1) -> ?ANIM_PHASE_2;
next_phase(_)             -> ?ANIM_PHASE_1.

