-module(gamerule).

-include("capturetheflag.hrl").

%% -export([new/0]). % World functions
-export([create_game_rule_update/0]).

-define(DEFAULT_BARRIER_HEALTH,    30 * ?FPS).
-define(DEFAULT_FLAG_HEALTH,       15 * ?FPS).
-define(DEFAULT_INVIS_HEALTH,      20 * ?FPS).
-define(DEFAULT_SPRINT_HEALTH,     200).
-define(DEFAULT_IMMORTAL_BARRIERS, false).
-define(DEFAULT_INVIS_MAX,         20 * ?FPS).
-define(DEFAULT_INVIS_MIN,         3 * ?FPS).
-define(DEFAULT_CAPTURE_LIMIT,     7).
-define(DEFAULT_TIME_LIMIT,        600 * ?FPS).
-define(DEFAULT_EXHAUST_TIME,      3 * ?FPS).
-define(DEFAULT_REACH,             40).
-define(DEFAULT_SPRINT_SPEED,      5).
-define(DEFAULT_WALK_SPEED,        3).
-define(DEFAULT_JAIL_TIME,         20 * ?FPS).
-define(DEFAULT_PAROLE_TIME,       10 * ?FPS).

%% ====================================================================
%% World Functions
%% ====================================================================

create_game_rule_update() ->
  {many, [{ie_set, {#gamerule.health_barrier,      ?DEFAULT_BARRIER_HEALTH}},
          {ie_set, {#gamerule.health_flag,         ?DEFAULT_FLAG_HEALTH}},
          {ie_set, {#gamerule.health_invis,        ?DEFAULT_INVIS_HEALTH}},
          {ie_set, {#gamerule.health_sprint,       ?DEFAULT_SPRINT_HEALTH}},
          {ie_set, {#gamerule.immortal_barriers,   ?DEFAULT_IMMORTAL_BARRIERS}},
          {ie_set, {#gamerule.invis_max_health,    ?DEFAULT_INVIS_MAX}},
          {ie_set, {#gamerule.invis_min_health,    ?DEFAULT_INVIS_MIN}},
          {ie_set, {#gamerule.limit_capture,       ?DEFAULT_CAPTURE_LIMIT}},
          {ie_set, {#gamerule.limit_time,          ?DEFAULT_TIME_LIMIT}},
          {ie_set, {#gamerule.max_exhaustion_time, ?DEFAULT_EXHAUST_TIME}},
          {ie_set, {#gamerule.reach,               ?DEFAULT_REACH}},
          {ie_set, {#gamerule.speed_sprint,        ?DEFAULT_SPRINT_SPEED}},
          {ie_set, {#gamerule.speed_walk,          ?DEFAULT_WALK_SPEED}},
          {ie_set, {#gamerule.time_jail,           ?DEFAULT_JAIL_TIME}},
          {ie_set, {#gamerule.time_parole,         ?DEFAULT_PAROLE_TIME}}]}.

