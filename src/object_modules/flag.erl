-module(flag).

-include("capturetheflag.hrl").
-include("timer.hrl").

-export([draw/3, new/0, tick/1]). %World functions
-export([can_capture/3, can_grab/2]). %Utility functions
-export([drop/2, grab/2, reset/1]). %Command functions
-export([new/2]). %Other functions

%% ====================================================================
%% World Functions
%% ====================================================================

draw(#flag{state = ?FLAG_POSS}, Canvas, _RenderOpts) -> Canvas;
draw(Flag, Canvas, RenderOpts) when not ?RADAR_ON(RenderOpts)->
  draw_flag(Flag, Canvas, RenderOpts);
draw(#flag{state = State, position = Position, timer = Timer} = Flag, Canvas, RenderOpts) ->
  Displace = ?DISPLACE(RenderOpts),
  C0 = case State of
         ?FLAG_DROP -> utilities:draw_gauge(Canvas, ?GREEN, ?DARKGREEN, 40, 6,
                                            ?TIMER_GETVALUE(Timer), ?TIMER_GETMAX(Timer),
                                            Displace(?Translate(Position, -10, -20)), false);
         _          -> Canvas
       end,
  draw_flag(Flag, C0, RenderOpts).

new() ->
  #flag{position = undefined,
        home     = undefined,
        state    = ?FLAG_HOME,
        team     = undefined,
        timer    = ?UPDATE(ctf_timer:new({gamerule, #gamerule.health_flag}, {gamerule, #gamerule.health_flag}, 0, -1, 1),
                           {set, {#ctf_timer.is_paused, true}}),
        who_has  = undefined}.

tick(#flag{state = ?FLAG_DROP, timer = Timer}) when ?TIMER_ISEXPIRED(Timer) ->
  {command, reset};
tick(#flag{state = ?FLAG_DROP, timer = Timer}) ->
  {#flag.timer, ctf_timer:tick(Timer)};
tick(_Flag) ->
  undefined.

%% ====================================================================
%% Utility Functions
%% ====================================================================

can_capture(#flag{state = ?FLAG_POSS,
                  who_has = WhoHas},
            #flag{state = ?FLAG_HOME,
                  home = Home},
            #player{player_id = WhoHas,
                    player_position = Position}) ->
  posn:is_near_rad(Position, Home, ?GETGAMERULE(#gamerule.reach));
can_capture(_Flag, _OtherFlag, _Player) -> false.

can_grab(#flag{state = ?FLAG_POSS}, _Position) ->
  false;
can_grab(#flag{position = FlagPosition}, Position) ->
  posn:is_near_rad(FlagPosition, Position, ?GETGAMERULE(#gamerule.reach)).

%% ====================================================================
%% Command Functions
%% ====================================================================

drop(_Flag, Posn) ->
  Dx = random:uniform(20) - 10,
  Dy = random:uniform(20) - 10,
  NewPosn = posn:displace(Posn, Dx, Dy),
  {many, [{set, {#flag.position, NewPosn}},
          {set, {#flag.state, ?FLAG_DROP}},
          {#flag.timer, {command, resume}}]}.

grab(_Flag, WhoHas) ->
  {many, [{set, {#flag.state, ?FLAG_POSS}},
          {set, {#flag.who_has, WhoHas}},
          {#flag.timer, {many, [{command, reset},
                                {command, pause}]}}]}.

reset(#flag{home = Home} = _Flag) ->
  {many, [{set, {#flag.position, Home}},
          {set, {#flag.state, ?FLAG_HOME}},
          {#flag.timer, {many, [{command, reset},
                                {command, pause}]}}]}.

%% ====================================================================
%% Other Functions
%% ====================================================================

new(Team, HomePosn) ->
  ?NEW({many, [{set, {#flag.position, HomePosn}},
               {set, {#flag.home,     HomePosn}},
               {set, {#flag.team,     Team}}]}).

%% ====================================================================
%% Internal functions
%% ====================================================================

draw_flag(#flag{position = Position,
                    team     = Team}, Canvas, RenderOpts) ->
  Path = case {?RADAR_ON(RenderOpts), Team} of
           {true, _} -> "resources/sprites/flags/greenflag-mini.png";
           {_, blue} -> "resources/sprites/flags/blueflag-mini.png";
           {_, red}  -> "resources/sprites/flags/redflag-mini.png"
         end,
  Displace = ?DISPLACE(RenderOpts),
  ?DrawImage(Canvas, Displace(?Translate(Position, -40, -40)), Path, [{mask, ?WHITE}]).
