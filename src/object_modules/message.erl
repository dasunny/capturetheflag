-module(message).

-include("capturetheflag.hrl").
-include("timer.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([add/2, new/1, tick/1]).
-export([]). %Command functions

add(Lom, Text) ->
    [new(Text) | Lom].

new(Text) ->
    #message{id = make_ref(),
             text = Text,
             timer = ctf_timer:new(?FPS*5, ?FPS*5, 0, -1, 1)}.

tick(Messages) ->
  {many, [tick_one(Message) || Message <- Messages]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

tick_one(#message{timer = Timer, id = Id}) when ?TIMER_ISEXPIRED(Timer) -> {id_delete, {#message.id, Id}};
tick_one(#message{timer = Timer}) -> {#message.timer, ctf_timer:tick(Timer)}.
