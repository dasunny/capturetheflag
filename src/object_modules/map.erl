-module(map).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([file_name/2, file_location/2, get_list/0, get_map_data_from_path/1, get_map_data_from_binary/1,
         load/1, pack/2, save_map_to_client/1]).
-export([unpack/1, map_exists/2]).

-define(Join, fun filename:join/1).

file_name(BaseName, Descriptor) ->
  lists:flatten(io_lib:format("~s_~s.png", [BaseName, Descriptor])).

file_location(BaseName, Descriptor) ->
  ?Join(["maps", BaseName, file_name(BaseName, Descriptor)]).

get_list() ->
  filelib:wildcard("maps/*.iem").

get_map_data_from_path(MapPath) ->
  MapBinary = load(MapPath),
  MapFiles = unpack(MapBinary),
  MapDataBinary = case lists:keyfind("map_data.dat", 1, MapFiles) of
                    {"map_data.dat", Binary} -> Binary;
                    _                        -> throw({error, map_data_not_found})
                  end,
  get_map_data_from_binary(MapDataBinary).

get_map_data_from_binary(MapDataBinary) ->
  case binary_to_term(MapDataBinary) of
    #map_data{} = MapData -> MapData;
    _                     -> throw({error, bad_map_data})
  end.

load(MapPath) ->
  io:format("loadingasdasd:~s\n",[MapPath]),
  Binary = case file:read_file(MapPath) of
             {ok, Bytes}    -> Bytes;
             {error, Error} -> throw({error, {error_reading_map, Error}})
           end,
  Binary.

map_exists(Name, Version) ->
  case file:read_file(?Join(["maps", Name, "map_data.dat"])) of
    {ok, Binary} -> #map_data{version = V} = get_map_data_from_binary(Binary),
                    V == Version;
    _            -> false
  end.

save_map_to_client(MapZipBinary) ->
  Files = unpack(MapZipBinary),
  MapData = case lists:keyfind("map_data.dat", 1, Files) of
              {"map_data.dat", Binary} -> get_map_data_from_binary(Binary);
              false                    -> throw({error, map_data_not_found})
            end,
  #map_data{name = Name} = MapData,
  ensure_directory(["maps", Name]),
  lists:foreach(fun(ZipMap) -> save_in(?Join(["maps", Name]), ZipMap) end, Files),
  MapData.

pack(MapName, Files) ->
  FileName = io_lib:format("maps/~s.iem", [MapName]),
  case zip:zip(FileName, Files, [memory]) of
    {ok, {FN, Bin}} -> save(FN, Bin);
    {error, Error} -> io:format("An error occured while packing the map file. Error: ~p\n", [Error]),
                      throw({error, packing_error, Error})
  end.

unpack(ArchiveBinary) ->
  case zip:unzip(ArchiveBinary, [memory]) of
    {ok, Files} -> Files;
    {error, Error} -> io:format("An error occured while unpacking a map file. Error: ~p\n", [Error]),
                      throw({error, unpacking_error, Error})
  end.

ensure_directory(Segments) ->
  Fun = fun(Next, SoFar) -> Acc = ?Join([SoFar, Next]),
                            case file:make_dir(Acc) of
                              ok -> ok;
                              {error, eexist} -> ok;
                              {error, Error} -> throw({error, path_segment_error, Error})
                            end,
                            Acc end,
  lists:foldl(Fun, ".", Segments).

save_in(Path, {FileName, Binary}) ->
  save_in(Path, FileName, Binary).
save_in(Path, FileName, Binary) ->
  save(?Join([Path, FileName]), Binary).

%% save({FileName, Binary}) ->
%%   save(FileName, Binary).
save(FileName, Binary) ->
  case file:write_file(FileName, Binary) of
    ok -> ok;
    {error, Error} -> io:format("An error occured while saving the map file. Error: ~p\n", [Error]),
                      throw({error, saving_error, Error})
  end.
