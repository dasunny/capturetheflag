-module(score).

-include("capturetheflag.hrl").

-export([new/0]). %World functions
-export([calculate/1]). %Utility functions
%% -export([precapture_plus/1, capture_plus/1, tag_plus/1, oppsave_plus/1, samesave_plus/1]). %Command functions

%% ====================================================================
%% World functions
%% ====================================================================

new() ->
  #score{pre_captures = 0,
         captures     = 0,
         tags         = 0,
         opp_saves    = 0,
         same_saves   = 0}.

%% ====================================================================
%% Command functions
%% ====================================================================

%% precapture_plus(#score{pre_captures = Captures}) ->
%%   {set, {pre_captures, Captures + 1}}.
%% 
%% capture_plus(#score{captures = Captures}) ->
%%   {set, {captures, Captures + 1}}.
%% 
%% tag_plus(#score{tags = Tags}) ->
%%   {set, {tags, Tags + 1}}.
%% 
%% oppsave_plus(#score{opp_saves = Saves}) ->
%%   {set, {opp_saves, Saves + 1}}.
%% 
%% samesave_plus(#score{same_saves = Saves}) ->
%%   {set, {same_saves, Saves + 1}}.

%% ====================================================================
%% Utility functions
%% ====================================================================

calculate(#score{pre_captures = Pres, captures = Caps, tags = Tags, opp_saves = OSaves, same_saves = SSaves}) ->
  Pres*3 + Caps*7 + Tags + OSaves*2 + SSaves.

%% ====================================================================
%% Internal functions
%% ====================================================================


