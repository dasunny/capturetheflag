-module(zone).

-include("capturetheflag.hrl").
-include("zone.hrl").

-export([draw/3, new/0, tick/1]). %World functions
-export([is_within/2, get_team_zones/2]). %Utility functions
-export([new/3]). %Other

%% ====================================================================
%% World Functions
%% ====================================================================

-define(SIDELENGTH,  15).
-define(BARRIER_MAX_LENGTH, 300).

draw(_Zone, Canvas, _RenderOpts) -> Canvas.

%% draw(#barrier{do_draw   = Draw,
%%               is_active = Active}, Canvas, _RenderOpts) when not Active orelse not Draw -> Canvas;
%% draw(#barrier{position = ?Posn(X, Y),
%%               timer    = Timer} = Barrier, Canvas, RenderOpts) ->
%%   Displace = ?DISPLACE(RenderOpts),
%%   C0 = draw_barrier(Barrier, Canvas, RenderOpts),
%%   C1 = case ?RADAR_ON(RenderOpts) of
%%          true  -> utilities:draw_gauge(C0, ?GREEN, ?DARKGREEN, 40, 6,
%%                                        ?TIMER_GETVALUE(Timer), ?TIMER_GETMAX(Timer), Displace(?Posn(X, Y-10)), false);
%%          false -> C0
%%        end,
%%   C1.

new() ->
  #zone{effect     = undefined,
        shape      = undefined,
        shape_data = undefined,
        team       = undefined}.

tick(_Zone) -> undefined.

%% ====================================================================
%% Utility Functions
%% ====================================================================

is_within(#zone{shape = Shape, shape_data = ShapeData} = _Zone, Position) ->
  is_within_int(Shape, ShapeData, Position).

get_team_zones(Zones, Team) ->
  [Zone || #zone{team = T} = Zone <- Zones, Team == T].

%% ====================================================================
%% Other Functions
%% ====================================================================

new(Team, Shape, ShapeData) ->
  ?NEW({many, [{set, {#zone.team, Team}},
               {set, {#zone.shape, Shape}},
               {set, {#zone.shape_data, ShapeData}}]}).

%% ====================================================================
%% Command Functions
%% ====================================================================

%% ====================================================================
%% Internal functions
%% ====================================================================

is_within_int(?ZONE_SHAPE_SQUARE, {TopLeft, Width, Height}, Posn) ->
  geometry:is_within_square(Posn, TopLeft, Width, Height);
is_within_int(?ZONE_SHAPE_CIRCLE, {Center, Radius}, Posn) ->
  geometry:is_within_circle(Posn, Center, Radius);
is_within_int(?ZONE_SHAPE_TRIANGLE, Points, Posn) ->
  geometry:is_within_triangle(Posn, Points).

