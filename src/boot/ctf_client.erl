-module(ctf_client).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([on_draw/3, on_tick/1, on_mouse/5, on_key/3, on_receive/2,
         on_resize/2, on_resize_pane/3, on_load/1, on_diag/1, start/0, start/1]).

start() ->
  case ?ServerChooser of
    error -> ok;
    {Host, Name} -> World = on_start({#world.player, {set, {#player.player_name, Name}}}),
                    start(World, Host)
  end.

start(Params) ->
  World = on_start(Params),
  start(World, localhost).

start(World, Host) ->
  ?IWorld(World, [{draw,        fun ?MODULE:on_draw/3},
                  {panes,       panes:get_starting_panes()},
                  {tick,        fun ?MODULE:on_tick/1},
                  {mouse,       fun ?MODULE:on_mouse/5},
                  {key,         fun ?MODULE:on_key/3},
                  {'receive',   fun ?MODULE:on_receive/2},
                  {resize,      fun ?MODULE:on_resize/2},
                  {resize_pane, fun ?MODULE:on_resize_pane/3},
                  {call_first,  fun ?MODULE:on_load/1},
                  {name,        World#world.player#player.player_name},
                  {register,    Host},
                  {clock_rate,  ?FPS},
                  {title,       "Capture the Flag!"},
                  {size,        {?GAMEWIDTH, ?GAMEHEIGHT}},
                  {start_hidden, ?PANE_ARENA},
                  {start_hidden, ?PANE_HUD},
                  {start_hidden, ?PANE_STATUS},
                  {start_hidden, ?PANE_PREGAME},
                  {start_hidden, ?PANE_POSTGAME}]).
%%                   {diagnostic, fun ?MODULE:on_diag/1}]).

on_start(InitalParams) ->
  ?UPDATE(world:new(), InitalParams).

on_draw(World, Canvas, Pane) ->
  on_draw(World, Canvas, Pane, World#world.phase).

on_key(World, Key, Type) ->
  on_key(World, Key, Type, World#world.phase).

on_mouse(#world{phase = ?PHASE_CHOOSING} = World, X, Y, Type, ?PANE_CHOOSING) ->
  update_mouse(World, X, Y, Type);
on_mouse(#world{phase = ?PHASE_PREGAME} = World, X, Y, Type, ?PANE_PREGAME) ->
  update_mouse(World, X, Y, Type);
on_mouse(#world{phase = ?PHASE_INGAME} = World, X, Y, Type, ?PANE_ARENA) ->
  update_mouse(World, X, Y, Type);
on_mouse(#world{phase = ?PHASE_POSTGAME} = World, X, Y, Type, ?PANE_POSTGAME) ->
  update_mouse(World, X, Y, Type);
on_mouse(World, _X, _Y, _Type, _Pane) ->
  World.

update_mouse(World, X, Y, Type) ->
  UpdatedWorld = World#world{mouse = mouse:update(World#world.mouse, X, Y, Type)},
  on_mouse2(UpdatedWorld, X, Y, Type, World#world.phase).

on_tick(World) ->
  world:tick(World).

on_receive(World, #idmail{mail = Message, id = _ID}) ->
%%   io:format("Player:\n~p\n",[World#world.player]), %%TODO
  ?UPDATE(World, Message);
on_receive(World, Message) ->
  ?UPDATE(World, Message).

on_resize(World, {NewWidth, NewHeight}) ->
  World#world{arena_size = {NewWidth-200,
                            NewHeight-100}}.

on_resize_pane(_World, NewSize, Pane) ->
  panes:resize_pane(Pane, NewSize).

on_load(_World) ->
  ok.

on_diag(World) -> world:diagnostic(World).

%% ====================================================================
%% Internal functions
%% ====================================================================

on_draw(World, Canvas, ?PANE_CHOOSING, ?PHASE_CHOOSING) ->
  ?PANE_CHOOSING:draw(World, Canvas);
on_draw(World, Canvas, Pane, ?PHASE_INGAME) when Pane == ?PANE_ARENA orelse
                                                   Pane == ?PANE_HUD orelse
                                                   Pane == ?PANE_STATUS ->
  Pane:draw(World, Canvas);
on_draw(_World, Canvas, _Pane, _Phase) ->
  Canvas.

on_key(World, Key, Type, ?PHASE_CHOOSING) -> perk_chooser:key(World, Key, Type);
on_key(World, Key, Type, _Phase)          -> world:key(World, Key, Type).

on_mouse2(World, X, Y, Type, ?PHASE_CHOOSING) -> perk_chooser:mouse(World, X, Y, Type);
on_mouse2(World, X, Y, Type, ?PHASE_INGAME)   -> world:mouse(World, X, Y, Type);
on_mouse2(World, _X, _Y, _Type, _Phase)       -> World.
