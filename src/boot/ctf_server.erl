-module(ctf_server).

-include("capturetheflag.hrl").
-include_lib("wx/include/wx.hrl").
-include("remote_config.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, on_tick/1, on_new/2, on_leave/2, on_message/3, on_diagnostic/1, on_load/1, config_setup/0]).

start() ->
  case map_chooser() of
    close     -> ok;
    {ok, Map} -> start(Map)
  end.
start(Map) ->
  Universe = ?UPDATE(universe:new(), {#universe.gamedata, {command, {apply_map, [Map]}}}),
  ?IUniverse(Universe, [{tick,       fun ?MODULE:on_tick/1},
                        {clock_rate, ?FPS},
                        {connect,    fun ?MODULE:on_new/2},
                        {disconnect, fun ?MODULE:on_leave/2},
                        {'receive',  fun ?MODULE:on_message/3},
                        {diagnostic, fun ?MODULE:on_diagnostic/1},
                        {call_first, fun ?MODULE:on_load/1},
                        {remote_config_setup, fun ?MODULE:config_setup/0}]).

on_leave(Universe, World) ->
  universe:leave(Universe, World).

on_message(Universe, World, Message) ->
  universe:message(Universe, World, Message).

on_new(Universe, World) ->
  universe:new(Universe, World).

on_tick(Universe) ->
  universe:tick(Universe).

on_diagnostic(Universe) ->
  universe:diagnostic(Universe).

on_load(#universe{gamedata = #gamedata{map_data = #map_data{spawns_blue = BlueSpawns,
                                                            spawns_red  = RedSpawns}}}) ->
  ?UPDATE(undefined, gamerule:create_game_rule_update()),
  utilities:set_respawn_positions(blue, BlueSpawns),
  utilities:set_respawn_positions(red,  RedSpawns).

config_setup() ->
  [?Column("Gamerule Settings", [?Section("Health Settings", [?Text("Barrier Health", "30"),
                                                              ?Text("Flag Health", "15"),
                                                              ?Text("Invisibility Health", "20"),
                                                              ?Text("Sprint Health", "200")]),
                                 ?Section("Game Settings", [?Spin("Capture Limit", 7, 1, 20),
                                                            ?Spin("Time Limit", 600, 60, 6000),
                                                            ?Spin("Jail Time Limit", 20, 1, 60),
                                                            ?Spin("Jail Time Limit", 10, 1, 60)]),
                                 ?CheckBox("Immortal Barriers", ["Enable"])]),
   ?Column("", [?Text("Sprint Recovery Time", 3),
                ?Section("Speed Settings", [?Spin("Walk Speed", 3, 1, 10),
                                            ?Spin("Sprint Speed", 5, 1, 20)]),
                ?Radio("Nothing really", ["Opt1",
                                          "Opt2",
                                          "Opt3"]),
                ?ListBox("Something else", ["choice1",
                                            "choice2",
                                            "choice3"])])].

%% ====================================================================
%% Internal functions
%% ====================================================================

map_chooser() ->
  Wx = wx:new(),
  Frame = wxFrame:new(Wx, ?wxID_ANY, "Map Selector", [{style, ?wxDEFAULT_FRAME_STYLE}]),
  Panel = wxPanel:new(Frame),
  Choose = wxButton:new(Panel, ?wxID_ANY, [{label, "Choose"}]),
  Cancel = wxButton:new(Panel, ?wxID_ANY, [{label, "Cancel"}]),
  Maps = [filename:basename(Filename, ".iem") || Filename <- filelib:wildcard("maps/*.iem")],
  ListBox = wxListBox:new(Panel, ?wxID_ANY, [{choices, Maps}, {style, ?wxLB_SINGLE}]),
  MainSizer = wxBoxSizer:new(?wxVERTICAL),
  ButtonSizer = wxBoxSizer:new(?wxHORIZONTAL),
  wxPanel:setSizer(Panel, MainSizer),
  wxSizer:add(MainSizer, ListBox, [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer, ButtonSizer, [{flag, ?wxEXPAND}]),
  wxSizer:add(ButtonSizer, Choose),
  wxSizer:add(ButtonSizer, Cancel),
  Self = self(),
  CB1 = fun(_,_) -> case wxListBox:getStringSelection(ListBox) of
                      ""  -> ok;
                      Map -> Self ! {ok, lists:flatten(io_lib:format("maps/~s.iem", [Map]))}
                    end end,
  CB2 = fun(_,_) -> Self ! close end,
  wxFrame:connect(Frame, close_window, [{callback, CB2}]),
  wxButton:connect(Choose, command_button_clicked, [{callback, CB1}]),
  wxButton:connect(Cancel, command_button_clicked, [{callback, CB2}]),
  {X,_} = wxButton:getDefaultSize(),
  wxFrame:setClientSize(Frame, {2*X, 3*X}),
  wxFrame:show(Frame),
  Out = receive
          Any -> Any
        end,
  wxFrame:hide(Frame),
  wx:destroy(),
  Out.
