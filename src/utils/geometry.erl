-module(geometry).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([is_within_square/4, is_within_circle/3, is_within_triangle/2]).

is_within_square(Point, TopLeft, Width, Height) ->
  ?Posn(Px, Py) = Point,
  ?Posn(TLx, TLy) = TopLeft,
  ?ISBETWEEN(TLx, Px, TLx+Width) andalso
    ?ISBETWEEN(TLy, Py, TLy + Height).

is_within_circle(Point, Center, Radius) ->
  posn:is_near_rad(Point, Center, Radius).

is_within_triangle(Point, {PointA, PointB, PointC}) ->
  Area = calculate_area(PointA,PointB, PointC),
  PAB  = calculate_area(Point, PointA, PointB),
  PBC  = calculate_area(Point, PointB, PointC),
  PAC  = calculate_area(Point, PointA, PointC),
  Area == PAB+PBC+PAC.



%% ====================================================================
%% Internal functions
%% ====================================================================

calculate_area(?Posn(Ax,Ay), ?Posn(Bx,By), ?Posn(Cx,Cy)) ->
  abs((Ax*(By-Cy) + Bx*(Cy-Ay) + Cx*(Ay-By)) / 2).
