-module(utilities).

-include("capturetheflag.hrl").

-export([new/2, update/2]). %Creation/modification
-export([set/2, reset/2, unset/2, dec/2, inc/2, 'not'/2, add_to_list/2, id_update/2, id_delete/2,
         ie_set/2, many/2, command/2]).
-export([convert_key/1, unit/1, set_respawn_positions/2, get_next_respawn_position/1, get_all_barriers/2]). %Utility
-export([cleanup/1, expand/1, collapse/1]). %Update utility
-export([draw_gauge/9, draw_reverse_gauge/9]). %Drawing utility
-export([get_color/1, get_other_color/1, get_dark_color/1, get_other_flag/1, get_other_team/1,
         get_background_color/1, team_to_string/1, team_to_string/2]). %Image utility

%% ====================================================================
%% Creation/modification
%% ====================================================================
new(Module, Update) ->
  Object = Module:new(),
  ?UPDATE(Object, Update).

update(Object, undefined) -> Object;

update(Object, {Key, Rest}) when is_integer(Key) ->
  set(Object, Key, update(element(Key,Object), Rest));
update(Object, {Op, Rest}) when is_atom(Op) ->
  try
    ?MODULE:Op(Object, Rest)
  catch
    C:E -> io:format("Update error: {~p,~p}\nStackTrace:\n~p\n",[C,E,erlang:get_stacktrace()]),
           throw({error, {bad_update, {Object, {Op, Rest}}}})
  end.

set(Object, Key, Value) ->
  setelement(Key, Object, Value).

%% Update sub functions

-define(IS_RESERVED(Key), Key == inc
          orelse Key == dec
          orelse Key == 'not'
          orelse Key == add_to_list
          orelse Key == set
          orelse Key == reset
          orelse Key == unset
          orelse Key == id_delete
          orelse Key == ie_set
          orelse Key == command).
%% -define(IS_NESTED(Key), is_integer(Key)
%%                  orelse Key == id_update
%%                  orelse Key == many).

set(Object, {Key, Value}) ->
  set(Object, Key, Value).
reset(Object, Key) -> set(Object, Key, 0).
unset(Object, Key) -> set(Object, Key, undefined).

dec(Object, {Key, Value}) -> increment(Object, Key, -Value);
dec(Object, Key)          -> increment(Object, Key, -1).
inc(Object, {Key, Value}) -> increment(Object, Key, Value);
inc(Object, Key)          -> increment(Object, Key, 1).
increment(Object, Key, Value) ->
  Old = element(Key, Object),
  set(Object, Key, Old + Value).

'not'(Object, Key) -> set(Object, Key, not element(Key, Object)).

add_to_list(Object, {Key, Value}) -> set(Object, Key, [Value | element(Key, Object)]).

id_update(Object, {{KeyLoc, KeyValue}, Update}) ->
  case lists:keyfind(KeyValue, KeyLoc, Object) of
    false     -> Object;
    MyObject -> UpdatedObject = update(MyObject, Update),
                lists:keyreplace(KeyValue, KeyLoc, Object, UpdatedObject)
  end.

id_delete(Object, {KeyLoc, KeyValue}) ->
  lists:keydelete(KeyValue, KeyLoc, Object).

ie_set(Object, {Key, Value}) ->
  ?SETGAMERULE(Key, Value),
  Object.

many(Object, Updates) ->
  Fun = fun(Elem, Acc) -> update(Acc, Elem) end,
  lists:foldl(Fun, Object, Updates).

command(Object, CommandArgs) ->
  Module = element(1, Object),
  Update = case CommandArgs of
             {Function, Args} -> erlang:apply(Module, Function, [Object | Args]);
             Function         -> erlang:apply(Module, Function, [Object])
           end,
  update(Object, Update).

%% ====================================================================
%% Utility
%% ====================================================================

convert_key(spacebar)  -> " ";
convert_key(backspace) -> backspace;
convert_key(delete)    -> delete;
convert_key(Character) ->
  Letter = atom_to_list(Character),
  case length(Letter) of
    1 -> Letter;
    _ -> ""
  end.

unit(X) when X >= 0 -> 1;
unit(_) -> -1.

-define(SpawnIndex, ctf_spawn_index).
-define(SpawnPositions, ctf_spawn_positions).

set_respawn_positions(Team, RespawnPositions) ->
  ?Set({?SpawnPositions, Team}, RespawnPositions),
  ?Set({?SpawnIndex, Team}, 1).

get_next_respawn_position(Team) ->
  RespawnIndex = ?Get({?SpawnIndex, Team}),
  RespawnPositions = ?Get({?SpawnPositions, Team}),
  ?Set({?SpawnIndex, Team}, ((RespawnIndex + 1) rem length(RespawnPositions)) + 1),
  lists:nth(RespawnIndex, RespawnPositions).

get_all_barriers(#gamedata{map_data = #map_data{field = Field}}, Players) ->
  FieldBarriers = [Barrier || #barrier{} = Barrier <- Field],
  PlayerBarriers = [Barrier || #player{barrier = #barrier{is_active = true,
                                                          position = Position} = Barrier} <- Players,
                               Position /= undefined],
  PlayerBarriers ++ FieldBarriers.
%%   get_barriers(#world{player = #player{player_perk = ?PERK_VAULT,
%%                                      player_team = Team},
%%                     gamedata = GameData}) ->
%%   gamedata:get_non_vaultable_field_barriers(GameData, Team);
%% get_barriers(#world{player = #player{barrier = Barrier,
%%                                      player_team = Team},
%%                     otherplayers = OtherPlayers,
%%                     gamedata = GameData}) ->
%%   OPBarriers = [Barrier || #player{barrier = #barrier{is_active = true,
%%                                                       position = Position} = Barrier} <- OtherPlayers,
%%                            Position /= undefined],
%%   FieldBarriers = gamedata:get_all_field_barriers(GameData, Team),
%%   [Barrier] ++ OPBarriers ++ FieldBarriers.

%% ====================================================================
%% Update Utility
%% ====================================================================

cleanup(Mails) ->
  Clean = cleanup_int(Mails),
  case Clean of
    Mails -> Clean;
    _     -> cleanup(Clean)
  end.

cleanup_int({id_update, {IdKey, Update}}) -> case cleanup(Update) of
                                               undefined -> undefined;
                                               Clean     -> {id_update, {IdKey, Clean}}
                                             end;
cleanup_int({Op, _Body} = Update)
  when ?IS_RESERVED(Op) -> Update;

cleanup_int({many, []})           -> undefined;
cleanup_int({many, [Many]})       -> cleanup(Many);
cleanup_int({many, Many})         -> {many, [cleanup(One) || One <- Many, One /= undefined]};
cleanup_int({_Header, undefined}) -> undefined;
cleanup_int({Header,  Update})    -> {Header, cleanup(Update)};
cleanup_int({_Update})            -> undefined;
cleanup_int(Update)               -> Update.

expand(Update) ->
  lists:sort([unlist(FlatUpdate) || FlatUpdate <- lists:flatten(expand_int(Update)), FlatUpdate /= undefined]).

expand_temp(Update) ->
  lists:flatten(expand_int(Update)).

expand_int({Header, _} = Update) when ?IS_RESERVED(Header) -> [Update];
expand_int({many, Updates}) -> expand_temp([expand_temp(Update) || Update <- Updates]);
expand_int({id_update, {{KeyLoc, Key}, Update}}) when is_list(Update)-> expand_temp([{id_update, {{KeyLoc, Key},
                                                                                                  expand_temp(Update)}} || Update <- Update]);
expand_int({id_update, {{KeyLoc, Key}, Update}}) -> expand_temp({id_update, {{KeyLoc, Key}, expand_temp(Update)}});
expand_int({Header, Update}) when is_list(Update) -> expand_temp([{Header, expand_temp(Update)} || Update <- Update]);
expand_int({Header, Update}) -> expand_temp({Header, expand_temp(Update)});
expand_int(Update) -> [Update].

unlist({Key, [Value]}) -> {Key, unlist(Value)};
unlist({id_update, {{KeyLoc, Key}, [Value]}}) -> {id_update, {{KeyLoc, Key}, unlist(Value)}};
unlist(Update) -> Update.

collapse([]) -> undefined;
collapse(undefined) -> undefined;
collapse(FlatUpdates) when is_list(FlatUpdates) ->
  Collapsed = collapse_int(lists:sort(FlatUpdates), undefined, [], []),
  ReExanded = reexpand(Collapsed),
  case ReExanded of
    [] -> undefined;
    [One] -> One;
    Many when is_list(Many) -> {many, lists:sort(Many)};
    Other -> Other
  end;
collapse(Update) -> Update.

collapse_int([], Last, AccAll, AccThis) ->
  All = [{Last, collapse(AccThis)} | AccAll],
  [Defined || {Header, _} = Defined <- All, Header /= undefined];
collapse_int([Update | Rest], Last, AccAll, AccThis) ->
  case Update of
    {Last, Body}   -> collapse_int(Rest, Last, AccAll, [Body | AccThis]);
    {Header, Body}
      when ?IS_RESERVED(Header) -> collapse_int(Rest, Last, [{Header, Body} | AccAll], AccThis);
    {Header, Body}
      when Last == undefined -> collapse_int(Rest, Header, AccAll, [Body]);
    {Header, Body} -> collapse_int(Rest, Header, [{Last, collapse(AccThis)} | AccAll], [Body]);
    undefined      -> collapse_int(Rest, Last, AccAll, AccThis);
    Other          -> collapse_int(Rest, Last, AccAll, [Other | AccThis])
  end.

reexpand(Collapsed) ->
  [reexpand_int(One) || One <- Collapsed, One /= undefined].

reexpand_int({id_update, {many, Headers}}) ->
  {many, [{id_update, Header} || Header <- reexpand(Headers)]};
reexpand_int({id_delete, {many, Headers}}) ->
  {many, [{id_delete, Header} || Header <- Headers]};
reexpand_int({Header, _} = Update) when ?IS_RESERVED(Header) ->
  Update;
reexpand_int({Header, {many, Updates}}) ->
  {Header, {many, [Update || Update <- reexpand(Updates)]}};
reexpand_int({Header, Body}) ->
  {Header, reexpand_int(Body)}.

%% ====================================================================
%% Drawing utility
%% ====================================================================

draw_reverse_gauge(Canvas, Front, Back, Width, Height, CurValue, MaxValue, Position, Disabled) ->
  draw_gauge(Canvas, Back, Front, Width, Height, MaxValue - CurValue, MaxValue, Position, Disabled).

draw_gauge(Canvas, ?COLOR(R1, G1, B1) = _Front, ?COLOR(R2, G2, B2) = _Back, Width, Height,
           CurValue, MaxValue, Position, true = _Disabled) ->
  C1 = trunc(R1*0.2 + G1*0.7 + B1*0.1),
  C2 = trunc(R2*0.2 + G2*0.7 + B2*0.1),
  draw_gauge_internal(Canvas, ?COLOR(C1, C1, C1), ?COLOR(C2, C2, C2), Width, Height,
                      CurValue, MaxValue, Position);

draw_gauge(Canvas, Front, Back, Width, Height, CurValue, MaxValue, Position, false = _Disabled) ->
  draw_gauge_internal(Canvas, Front, Back, Width, Height, CurValue, MaxValue, Position).

%% ====================================================================
%% Image utility
%% ====================================================================

get_color(blue) -> ?BLUE;
get_color(red)  -> ?RED;
get_color(_)    -> ?BLACK.

get_other_color(blue) -> ?RED;
get_other_color(red)  -> ?BLUE;
get_other_color(_)    -> ?BLACK.

get_dark_color(blue) -> ?DARKBLUE;
get_dark_color(red)  -> ?DARKRED;
get_dark_color(_)    -> ?ORANGE.

get_other_flag(blue) -> #gamedata.flag_red;
get_other_flag(red)  -> #gamedata.flag_blue.

get_other_team(blue) -> red;
get_other_team(red)  -> blue;
get_other_team(_)    -> black.

get_background_color(true) -> ?BLACK;
get_background_color(false) -> ?WHITE;
get_background_color(#world{player = #player{is_radar = true}}) -> ?BLACK;
get_background_color(_) -> ?WHITE.

team_to_string(Team) -> team_to_string(Team, false).
team_to_string(_Team, true) -> "green";
team_to_string(blue, false) -> "blue";
team_to_string(red, false)  -> "red".

%% ====================================================================
%% Internal functions
%% ====================================================================
draw_gauge_internal(Canvas, Front, _Back, Width, Height, CurValue, CurValue, Position) ->
  ?DrawRectangle(Canvas, Position, Width, Height, Front);
draw_gauge_internal(Canvas, Front, Back, Width, Height, CurValue, MaxValue, Position) ->
  C0 = ?DrawRectangle(Canvas, Position, Width, Height, Back),
  NewWidth = trunc(CurValue/MaxValue * Width),
  C1 = ?DrawRectangle(C0, Position, max(0, NewWidth), Height, Front),
  C1.
