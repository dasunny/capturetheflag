-module(ctf_compiler).

-include_lib("kernel/include/file.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).

start() ->
  FileList = get_file_list("src", ".erl"),
  io:format("Found files:\n~p\n",[FileList]),
  io:format("Found ~p .erl files\n",[length(FileList)]),
  Fun = fun(One, {Ok, Error}) -> case compile_one(One) of
                                   ok    -> {Ok+1, Error};
                                   {error, Name} -> {Ok, [Name | Error]}
                                 end end,
  {Ok, Error} = lists:foldl(Fun, {0,[]}, lists:sort(FileList)),
  io:format("Successfully compiled ~p of ~p files.\n", [Ok, length(FileList)]),
  Error == [] orelse io:format("The following files did not compile:\n\t~s\n",
                               [lists:flatten(lists:join("\n\t", lists:sort(Error)))]),
  ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

get_file_list(Path, Ext) ->
  {ok, List} = file:list_dir(Path),
  Joined = [filename:join(Path, File) || File <- List],
  IsDir = fun(File) -> {ok, Info} = file:read_file_info(File),
                       Info#file_info.type == directory end,
  IsExt = fun(File) -> filename:extension(File) == Ext end,
  Fun = fun(Elem, Acc) -> case IsDir(Elem) of
                            true  -> get_file_list(Elem, Ext) ++ Acc;
                            false -> case IsExt(Elem) of
                                       true  -> [Elem | Acc];
                                       false -> Acc
                                     end
                          end end,
  lists:foldl(Fun, [], Joined).

compile_one(File) ->
  case c:c(File, [{outdir, "ebin"},
                  {i, "../"},
                  {i, "include/"},
                  {i, "ie_include/"},
                  {i, "../interactive_erlang/include/"}]) of
    {ok, Module} -> io:format("Succesfully compiled ~p\n", [Module]),
                    ok;
    _Error       -> io:format("Unsuccessfully compiled ~s\n\n",[filename:basename(File, ".erl")]),
                    {error, File}
  end.