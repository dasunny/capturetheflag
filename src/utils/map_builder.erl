-module(map_builder).

-include("capturetheflag.hrl").
-include("barrier.hrl").
-include("zone.hrl").
-include_lib("wx/include/wx.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).

-define(Type_Null, type_null).
-define(Type_Wall,          {type_wall, _}).
-define(Type_Wall_Square,   {type_wall, square}).
-define(Type_Wall_Triangle, {type_wall, triangle}).
-define(Type_Wall_Circle,   {type_wall, circle}).
-define(Type_Wall_Zone_Blue, {type_wall, {zone, blue}}).
-define(Type_Wall_Zone_Red,  {type_wall, {zone, red}}).
-define(Type_Wall_Zone,      {type_wall, {zone, _}}).
-define(Type_Wall_Vault,      {type_wall_vault, _}).
-define(Type_Wall_Vault_All,  {type_wall_vault, all}).
-define(Type_Wall_Vault_Blue, {type_wall_vault, blue}).
-define(Type_Wall_Vault_Red,  {type_wall_vault, red}).
-define(Type_Air_Zone_Blue, {type_air, {zone, blue}}).
-define(Type_Air_Zone_Red,  {type_air, {zone, red}}).
-define(Type_Air_Zone,      {type_air, {zone, _}}).
-define(Type_Zone,      {_, {zone, _}}).
-define(Type_Zone_Blue, {_, {zone, blue}}).
-define(Type_Zone_Red,  {_, {zone, red}}).
-define(Type_Respawn_Blue, type_respawn_blue).
-define(Type_Respawn_Red,  type_respawn_red).
-define(Type_Home_Blue, type_home_blue).
-define(Type_Home_Red,  type_home_red).

-record(map_pixel, {xy,
                    rgb,
                    type,
                    tile_key}).

-record(map_tile, {xyr,
                   image,
                   bitmap}).

-define(Mask, {255,128,255}).

-define(Rand(N), rand:uniform(N)).
-define(Rand0(N), ?Rand(N)-1).
-define(Rand(N, M), N + rand:uniform(M-N)).
-define(Rand0(N, M), ?Rand(N,M)-1).

-define(Print(String), print("~s\n",[String])).
-define(Print(Fmt, Args), print(Fmt, Args)).

start() ->
  Wx = wx:new(),
  Frame = wxFrame:new(Wx, ?wxID_ANY, "CTF Map Builder", [{style, ?wxDEFAULT_FRAME_STYLE}]),
  Panel = wxPanel:new(Frame),
  BuildButton = wxButton:new(Panel, ?wxID_ANY, [{label, "Build Map"}]),
  CheckBox = wxCheckBox:new(Panel, ?wxID_ANY, "Delete Build Artifacts"),
  LogText = wxTextCtrl:new(Panel, 100, [{style, ?wxTE_MULTILINE}]),
  MainSizer = wxBoxSizer:new(?wxVERTICAL),
  LogSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, []),

  Fun = fun(Label, Path, Default) -> Sizer  = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, [{label, Label}]),
                            Text   = wxTextCtrl:new(Panel, ?wxID_ANY, [{value, Default}]),
                            Button = wxButton:new(Panel, ?wxID_ANY, [{label, "Choose File"}]),
                            wxSizer:add(MainSizer, Sizer, [{flag, ?wxEXPAND}]),
                            wxSizer:add(Sizer, Text, [{proportion, 1}]),
                            wxSizer:add(Sizer, Button, []),
                            Dialog = wxFileDialog:new(Panel, [{defaultDir, Path},
                                                              {style, ?wxFD_OPEN bor ?wxFD_FILE_MUST_EXIST}]),
                            Callback = fun(_,_) -> case wxFileDialog:showModal(Dialog) of
                                                     ?wxID_OK -> RetPath = wxFileDialog:getPath(Dialog),
                                                                 wxTextCtrl:setValue(Text, RetPath),
                                                                 ok;
                                                     ?wxID_CANCEL -> ok
                                                   end end,
                            wxButton:connect(Button, command_button_clicked, [{callback, Callback}]),
                            Text end,
  MapPath   = Fun("Map File", "maps/uncompiled/", ""),
  TilePath  = Fun("Normal View Tile File", "resources/map_resources/", ""),
  RadarPath = Fun("Radar View Tile File", "resources/map_resources/", "resources/map_resources/radar_tiles.png"),
  wxSizer:add(MainSizer, BuildButton, [{flag, ?wxEXPAND}]),
  wxSizer:add(MainSizer, CheckBox, [{flag, ?wxEXPAND}]),
  wxSizer:add(LogSizer, LogText, [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer, LogSizer, [{flag, ?wxEXPAND}, {proportion, 1}]),

  wxButton:connect(BuildButton, command_button_clicked, [{callback, fun(_,_) -> build(wxTextCtrl:getValue(MapPath),
                                                                                      wxTextCtrl:getValue(TilePath),
                                                                                      wxTextCtrl:getValue(RadarPath),
                                                                                      wxCheckBox:getValue(CheckBox)) end}]),
  Self = self(),
  wxFrame:connect(Frame, close_window, [{callback, fun(_,_) -> Self ! close end}]),

  wxPanel:setSizer(Panel, MainSizer),
  wxFrame:setClientSize(Frame, {400, 600}),
  wxFrame:show(Frame),
  receive
    close -> ok
  end,
  wxFrame:hide(Frame),
  wx:destroy().

build(MapPath, TilePath, RadarPath, DeleteArtifacts) ->
  try
    ?Print("Building map at ~s with resource set ~s\n",[MapPath, TilePath]),
    ?Print("Parsing map..."),
    Pixels = read_map_pixels(MapPath),
    ?Print("Parsing tiles..."),
    Tiles  = read_map_tiles(TilePath),
    RadarTiles  = read_map_tiles(RadarPath),
    MinimapTiles = read_map_tiles("resources/map_resources/minimap_tiles.png"),

    {BlueSpawns, RedSpawns} = get_respawns(Pixels),
    {BlueHome, RedHome} = get_flag_homes(Pixels),
    Barriers = create_barriers(Pixels),
    Zones = create_zones(Pixels),

    ?Print("Building Arena Image"),
    ArenaImage = create_tile_image(MapPath, Pixels, Tiles, DeleteArtifacts, "arena"),
    ?Print("Building Radar Image"),
    RadarImage = create_tile_image(MapPath, Pixels, RadarTiles, DeleteArtifacts, "radar"),
    ?Print("Building Minimap"),
    MinimapImage = create_tile_image(MapPath, Pixels, MinimapTiles, DeleteArtifacts, "mini", 0.25),
    MapName = filename:basename(MapPath, ".png"),
    {MapX, MapY} = (lists:last(lists:keysort(#map_pixel.xy, Pixels)))#map_pixel.xy,
    MapData = #map_data{field       = Barriers,
                        home_blue   = BlueHome,
                        home_red    = RedHome,
                        size        = {(MapX+1)*50, (MapY+1)*50},
                        name        = MapName,
                        spawns_blue = BlueSpawns,
                        spawns_red  = RedSpawns,
                        version     = calendar:local_time(),
                        zones       = Zones},
    MapDataBinary = term_to_binary(MapData),
    Files = [{"map_data.dat", MapDataBinary},
             {map:file_name(MapName, "arena"), ArenaImage},
             {map:file_name(MapName, "radar"), RadarImage},
             {map:file_name(MapName, "mini"), MinimapImage}],
    map:pack(MapName, Files),
    case DeleteArtifacts of
      true  -> file:del_dir(filename:join(["maps", ".build_artifacts", MapName]));
      false -> ok
    end,
    ?Print("Done!"),
    ok
  catch
    _:Error -> ?Print("Caught an error! ~p\nStacktrace:\n~p\n",[Error, erlang:get_stacktrace()]),
               Error
  end.

%% ====================================================================
%% Internal functions
%% ====================================================================

print(Fmt, Args) ->
  Window = wxWindow:findWindowById(100),
  AsTextCtrl = wx:typeCast(Window, wxTextCtrl),
  Text = io_lib:format(Fmt, Args),
  wxTextCtrl:appendText(AsTextCtrl, Text),
  ok.

read_map_pixels(Path) ->
  ?Print("Reading map at ~s\n",[Path]),
  Image = wxImage:new(Path),
  Width = wxImage:getWidth(Image),
  Height = wxImage:getHeight(Image),
  Seq = create_seq(Width, Height),
  ImageData = wxImage:getData(Image),
  GetRGB = fun({X, Y}) -> Start = (X + Width*Y)*3,
                        <<_Head:Start/binary, R:8, G:8, B:8, _Rest/binary>> = ImageData,
                        #map_pixel{xy  = {X, Y},
                                   rgb = {R, G, B},
                                   type = get_pixel_type(R, G, B)} end,
  ?Print("Reading map with dimensions ~px~p\n",[Width,Height]),
  PrePixels = lists:map(GetRGB, Seq),
  [add_tile_keys(Pixel, PrePixels) || Pixel <- PrePixels].

read_map_tiles(TilePath) ->
  Image = wxImage:new(TilePath),
  Width = wxImage:getWidth(Image),
  Height = wxImage:getHeight(Image),
  Seq = create_seq(Width div 27, Height div 27),
  Fun = fun({X,Y}) -> SubImage = wxImage:getSubImage(Image, {X*27+1, Y*27+1, 25, 25}),
                      #map_tile{xyr = {{X,Y},0},
                                image = SubImage,
                                bitmap = wxBitmap:new(SubImage)} end,
  MapTiles = [Fun(XY) || XY <- Seq],
  wxImage:destroy(Image),
  MapTiles.

get_pixel_type(255, 255, 255) -> ?Type_Null;
get_pixel_type(  0,   0,   0) -> ?Type_Wall_Square;
get_pixel_type( 64,  64,  64) -> ?Type_Wall_Triangle;
get_pixel_type(192, 192, 192) -> ?Type_Wall_Circle;
get_pixel_type(  N,   N,   N) when N == 127; N == 128 -> ?Type_Wall_Vault_All;
get_pixel_type(  N,   N, 255) when N == 127; N == 128 -> ?Type_Wall_Vault_Blue;
get_pixel_type(255,   N,   N) when N == 127; N == 128 -> ?Type_Wall_Vault_Red;
get_pixel_type(255,   0,   0) -> ?Type_Respawn_Red;
get_pixel_type(  0,   0, 255) -> ?Type_Respawn_Blue;
get_pixel_type(128,   0,   0) -> ?Type_Home_Red;
get_pixel_type(  0,   0, 128) -> ?Type_Home_Blue;
get_pixel_type( 64,  64, 128) -> ?Type_Wall_Zone_Blue;
get_pixel_type(128,  64,  64) -> ?Type_Wall_Zone_Red;
get_pixel_type( 64,  64, 255) -> ?Type_Air_Zone_Blue;
get_pixel_type(255,  64,  64) -> ?Type_Air_Zone_Red.

add_tile_keys(#map_pixel{type = ?Type_Wall_Vault_All} = Pixel, _Pixels) ->
  Pixel#map_pixel{tile_key = {{1,1}, ?Rand0(4)}};
add_tile_keys(#map_pixel{type = ?Type_Wall_Vault_Blue} = Pixel, _Pixels) ->
  Pixel#map_pixel{tile_key = {{1,2}, ?Rand0(4)}};
add_tile_keys(#map_pixel{type = ?Type_Wall_Vault_Red} = Pixel, _Pixels) ->
  Pixel#map_pixel{tile_key = {{1,3}, ?Rand0(4)}};
add_tile_keys(#map_pixel{type = ?Type_Home_Blue} = Pixel, _Pixels) ->
  Pixel#map_pixel{tile_key = {{0,2}, 0}};
add_tile_keys(#map_pixel{type = ?Type_Home_Red} = Pixel, _Pixels) ->
  Pixel#map_pixel{tile_key = {{0,3}, 0}};
add_tile_keys(Pixel, Pixels) ->
  #map_pixel{xy = {X,Y},
             type = Type} = Pixel,
  Get = fun(Dx, Dy) -> case lists:keyfind({X+Dx, Y+Dy}, #map_pixel.xy, Pixels) of
                         #map_pixel{type = ?Type_Wall} -> 1;
                         #map_pixel{}                  -> 0;
                         false                         -> 1
                       end end,
  Set = {Get(-1,-1), Get(0,-1), Get(1,-1),
         Get(-1, 0), Get(0, 0), Get(1, 0),
         Get(-1, 1), Get(0, 1), Get(1, 1)},
  Pixel#map_pixel{tile_key = get_tile_key(Type, Set)}.

get_tile_key(_Type, {_,_,_,_,0,_,_,_,_}) -> undefined;
get_tile_key(_Type, {_,0,_,0,1,0,_,0,_}) -> {{0,0}, 0};

get_tile_key(_Type, {_,A,_,B,1,C,_,D,_}) when A+B+C+D==1 ->
  Rotation = case {A,B,C,D} of
               {1,_,_,_} -> 2;
               {_,1,_,_} -> 1;
               {_,_,1,_} -> 3;
               {_,_,_,1} -> 0
             end,
  {{1,0}, Rotation};

%% get_tile_key(_Type, {_,1,_,0,1,0,_,1,_}) -> {{2,0}, 0 + 2*?Rand0(2)};
%% get_tile_key(_Type, {_,0,_,1,1,1,_,0,_}) -> {{2,0}, 1 + 2*?Rand0(2)};
get_tile_key(_Type, {_,1,_,0,1,0,_,1,_}) -> {{2,0}, 0};
get_tile_key(_Type, {_,0,_,1,1,1,_,0,_}) -> {{2,0}, 1};

get_tile_key(Type, {W,A,X,B,1,C,Y,D,Z}) when A+B+C+D==2 ->
  Not = fun(0) -> 1;
           (1) -> 0 end,
  Rotation = case {A,B,C,D} of
               {_,_,1,1} -> 0;
               {_,1,_,1} -> 1;
               {1,1,_,_} -> 2;
               {1,_,1,_} -> 3
             end,
  XCo = case Type of
          ?Type_Wall_Square -> 3;
          ?Type_Wall_Triangle -> 7;
          ?Type_Wall_Circle -> 8
        end,
  YCo = case Rotation of
          0 -> Not(Z);
          1 -> Not(Y);
          2 -> Not(W);
          3 -> Not(X)
        end,
  {{XCo,YCo}, Rotation};

get_tile_key(_Type, {W,A,X,B,1,C,Y,D,Z}) when A+B+C+D==3 ->
  Rotation = case {A,B,C,D} of
               {0,_,_,_} -> 0;
               {_,_,0,_} -> 1;
               {_,_,_,0} -> 2;
               {_,0,_,_} -> 3
             end,
  YCo = case {Rotation,W,X,Y,Z} of
          {0, _,_,1,1} -> 0;
          {0, _,_,1,0} -> 1;
          {0, _,_,0,1} -> 2;
          {0, _,_,0,0} -> 3;
          {1, 1,_,1,_} -> 0;
          {1, 1,_,0,_} -> 1;
          {1, 0,_,1,_} -> 2;
          {1, 0,_,0,_} -> 3;
          {2, 1,1,_,_} -> 0;
          {2, 0,1,_,_} -> 1;
          {2, 1,0,_,_} -> 2;
          {2, 0,0,_,_} -> 3;
          {3, _,1,_,1} -> 0;
          {3, _,0,_,1} -> 1;
          {3, _,1,_,0} -> 2;
          {3, _,0,_,0} -> 3
        end,
  {{4,YCo}, Rotation};

get_tile_key(_Type, {A,1,B,1,1,1,C,1,D}) ->
  {YCo, Rot} = case {A,B,C,D} of
                 {0,0,0,0} -> {1, ?Rand0(4)};
                 {0,0,0,1} -> {2, 3};
                 {0,0,1,0} -> {2, 0};
                 {0,0,1,1} -> {3, 2};
                 {0,1,0,0} -> {2, 2};
                 {0,1,0,1} -> {3, 1};
%%                  {0,1,1,0} -> {4, 2*?Rand0(2)};
                 {0,1,1,0} -> {4, 0};
                 {0,1,1,1} -> {5, 2};
                 {1,0,0,0} -> {2, 1};
%%                  {1,0,0,1} -> {4, 1 + 2*?Rand0(2)};
                 {1,0,0,1} -> {4, 1};
                 {1,0,1,0} -> {3, 3};
                 {1,0,1,1} -> {5, 3};
                 {1,1,0,0} -> {3, 0};
                 {1,1,0,1} -> {5, 1};
                 {1,1,1,0} -> {5, 0};
                 {1,1,1,1} -> {0, 0}
               end,
  {{5,YCo}, Rot}.

to_posn({X,Y}) ->
  X50 = fun(N) -> N*50 + 25 end,
  ?Posn(X50(X), X50(Y)).

get_respawns(Pixels) ->
  Blues = [to_posn(XY) || #map_pixel{xy = XY, type = ?Type_Respawn_Blue} <- Pixels],
  Reds  = [to_posn(XY) || #map_pixel{xy = XY, type = ?Type_Respawn_Red}  <- Pixels],
  Blues == [] andalso throw({error, no_blue_respawns}),
  Reds == []  andalso throw({error, no_red_respawns}),
  {Blues, Reds}.

%% average_flag_home(FlagPixels) ->
%%   Average = fun(XY, {AccX,AccY}) -> ?Posn(X,Y) = to_posn(XY),
%%                                                      {AccX+X,AccY+Y} end,
%%   {AverageX, AverageY} = lists:foldl(Average, {0,0}, FlagPixels),
%%   Num = length(FlagPixels),
%%   ?Posn(AverageX div Num, AverageY div Num).

get_flag_homes(Pixels) ->
  Blue = case [to_posn(XY) || #map_pixel{xy = XY, type = ?Type_Home_Blue} <- Pixels] of
           []  -> ?Print("No Blue Flag Home found!"),
                    throw({error, no_blue_flag_home});
           [B] -> B;
           _   -> ?Print("More than 1 Blue Flag Home found!"),
                  throw({error, too_many_blue_flag_homes})
%%            Blues -> average_flag_home(Blues)
         end,
  Red = case [to_posn(XY) || #map_pixel{xy = XY, type = ?Type_Home_Red} <- Pixels] of
          []  -> ?Print("No Red Flag Home found!"),
                 throw({error, no_red_flag_home});
          [R] -> R;
          _   -> ?Print("More than 1 Red Flag Home found!"),
                 throw({error, too_many_red_flag_homes})
%%           Reds -> average_flag_home(Reds)
        end,
  {Blue, Red}.

create_barriers(Pixels) ->
  WallPixels     = [Pixel || #map_pixel{type = ?Type_Wall}          = Pixel <- Pixels],
  SquarePixels   = [Pixel || #map_pixel{type = ?Type_Wall_Square}   = Pixel <- WallPixels],
  TrianglePixels = [Pixel || #map_pixel{type = ?Type_Wall_Triangle} = Pixel <- WallPixels],
  CirclePixels   = [Pixel || #map_pixel{type = ?Type_Wall_Circle}   = Pixel <- WallPixels],
  VaultPixels    = [Pixel || #map_pixel{type = ?Type_Wall_Vault}    = Pixel <- WallPixels],
  WallZonePixels = [Pixel || #map_pixel{type = ?Type_Wall_Zone}     = Pixel <- WallPixels],

  SquareBarriers   = create_square_barriers(SquarePixels ++ WallZonePixels, []),
  TriangleBarriers = create_triangle_barriers(TrianglePixels, WallPixels),
  CircleBarriers   = create_circle_barriers(CirclePixels, WallPixels),
  VaultBarriers    = create_vault_barriers(VaultPixels),
  SquareBarriers ++ TriangleBarriers ++ CircleBarriers ++ VaultBarriers ++ WallZonePixels.

create_square_barriers([], AccBarriers) -> AccBarriers;
create_square_barriers([#map_pixel{xy = {X, Y}} = Pixel | Rest], AccBarriers) ->
  {Dx, Dy} = find_best_match(Pixel, Rest),
  Seq = [{M,N} || M <- lists:seq(X, X+Dx-1, utilities:unit(Dx)),
                  N <- lists:seq(Y, Y+Dy-1, utilities:unit(Dy))],
  Fun = fun(Elem, {AccPixels, AccRest} = Acc) -> case lists:keytake(Elem, #map_pixel.xy, AccRest) of
                                             false -> Acc;
                                             {value, Taken, Without} -> {[Taken | AccPixels], Without}
                                           end end,
  {InThisBarrier, NewRest} = lists:foldr(Fun, {[Pixel], Rest}, Seq),
  [#map_pixel{xy = {FirstX, FirstY}} | _] = lists:keysort(#map_pixel.xy, InThisBarrier),
  NewBarrier = barrier:new_immortal(?Posn(FirstX*50,FirstY*50), abs(Dx)*50, abs(Dy)*50, ?BARRIER_VAULT_NONE),
  create_square_barriers(NewRest, [NewBarrier | AccBarriers]).

create_triangle_barriers(Pixels, WallPixels) ->
  ToBarrier = fun(#map_pixel{xy = {X,Y}} = Pixel) -> Orientation = find_orientation(Pixel, WallPixels),
                                                     New = barrier:new_immortal(?Posn(X*50,Y*50), 50, 50, ?BARRIER_VAULT_NONE),
                                                     ?UPDATE(New, {many, [{set, {#barrier.shape, ?BARRIER_SHAPE_TRIANGLE}},
                                                                          {set, {#barrier.orientation, Orientation}}]}) end,
  [ToBarrier(Pixel) || Pixel <- Pixels].

create_circle_barriers(Pixels, WallPixels) ->
  ToBarrier = fun(#map_pixel{xy = {X,Y}} = Pixel) -> Orientation = find_orientation(Pixel, WallPixels),
                                                     New = barrier:new_immortal(?Posn(X*50,Y*50), 50, 50, ?BARRIER_VAULT_NONE),
                                                     ?UPDATE(New, {many, [{set, {#barrier.shape, ?BARRIER_SHAPE_CIRCLE}},
                                                                          {set, {#barrier.orientation, Orientation}}]}) end,
  [ToBarrier(Pixel) || Pixel <- Pixels].

create_vault_barriers(Pixels) ->
  All  = [Pixel || #map_pixel{type = ?Type_Wall_Vault_All}  = Pixel <- Pixels],
  Blue = [Pixel || #map_pixel{type = ?Type_Wall_Vault_Blue} = Pixel <- Pixels],
  Red  = [Pixel || #map_pixel{type = ?Type_Wall_Vault_Red}  = Pixel <- Pixels],
  Convert = fun(MyPixels, Type) -> Barriers = create_square_barriers(MyPixels, []),
                                [?UPDATE(Barrier, {set, {#barrier.vault_type, Type}}) || Barrier <- Barriers] end,
  Convert(All, ?BARRIER_VAULT_ALL) ++ Convert(Blue, ?BARRIER_VAULT_BLUE) ++ Convert(Red, ?BARRIER_VAULT_RED).

find_best_match(#map_pixel{xy = XY}, Rest) ->
  SpanUp    = count_straight_span(XY, { 0,-1}, Rest),
  SpanDown  = count_straight_span(XY, { 0, 1}, Rest),
  SpanLeft  = count_straight_span(XY, {-1, 0}, Rest),
  SpanRight = count_straight_span(XY, { 1, 0}, Rest),
  SpanUpRight    = count_box_span(XY, { 1,-1}, Rest),
  SpanDownRight  = count_box_span(XY, { 1, 1}, Rest),
  SpanDownLeft   = count_box_span(XY, {-1, 1}, Rest),
  SpanUpLeft     = count_box_span(XY, {-1,-1}, Rest),
  {_N, Dxy} = lists:max([SpanUp,SpanDown,SpanLeft,SpanRight,
                         SpanUpRight,SpanDownRight,SpanDownLeft,SpanUpLeft]),
  Dxy.

count_straight_span(Start, Add, Pixels) ->
  count_straight_span(Start, Add, Pixels, 1).
count_straight_span({X,Y} = Start, {Dx,Dy} = DXY, Pixels, N) ->
  case is_occupied({X+(Dx)*N,Y+(Dy)*N}, Pixels) of
    true  -> count_straight_span(Start, DXY, Pixels, N+1);
    false -> {N, {Dx*N,Dy*N}}
  end.

count_box_span({X, Y} = Start, Add, Pixels) ->
  {Dx,Dy} = Add,
  {MaxHorizontalSpan, _} = count_straight_span(Start, {Dx,0}, Pixels),
  {MaxVerticalSpan,   _} = count_straight_span(Start, {0,Dy}, Pixels),
  Seq = [{A,B} || A <- lists:seq(1, MaxHorizontalSpan, abs(Dx)),
                  B <- lists:seq(1, MaxVerticalSpan, abs(Dy))],
  AllOccupied = fun({A,B}) -> Seq2 = [{X+M*Dx,
                                       Y+N*Dy} || M <- lists:seq(1, A-1),
                                                  N <- lists:seq(1, B-1)],
                              Out = lists:all(fun(Elem) -> is_occupied(Elem, Pixels) end, Seq2),
                              Out end,
  Valids = [{A*B, AB} || {A,B} = AB <- Seq, AllOccupied(AB)],
  lists:max(Valids).

create_zones(Pixels) ->
  ZonePixels = [Pixel || #map_pixel{type = ?Type_Zone} = Pixel <- Pixels],
  BluePixels = [Pixel || #map_pixel{type = ?Type_Zone_Blue} = Pixel <- ZonePixels],
  RedPixels  = [Pixel || #map_pixel{type = ?Type_Zone_Red} = Pixel <- ZonePixels],
  Fun = fun(Team, TeamPixels) -> case [XY || #map_pixel{xy = XY} <- TeamPixels] of
                                   [{X1, Y1}, {X2, Y2}] -> XMin = min(X1,X2),
                                                           XMax = max(X1,X2),
                                                           YMin = min(Y1,Y2),
                                                           YMax = max(Y1,Y2),
                                                           zone:new(Team, ?ZONE_SHAPE_SQUARE,
                                                                    {?Posn(XMin*50, YMin*50), (XMax+1-XMin)*50, (YMax+1-YMin)*50});
                                   Else                 -> ?Print("Unsupported Zone format! There may only be 2 zone pixels of any color. "
                                                                  "Found ~p ~p zone pixels at ~p\n", [length(Else), Team, Else]),
                                                           throw({error, {bad_zone, Team}})
                                 end end,
  BlueZone = Fun(blue, BluePixels),
  RedZone  = Fun(red, RedPixels),
  [BlueZone, RedZone].

is_occupied(XY, Pixels) -> lists:keymember(XY, #map_pixel.xy, Pixels).

find_orientation(#map_pixel{xy = {X,Y}}, WallPixels) ->
  Up    = lists:keyfind({X,  Y-1}, #map_pixel.xy, WallPixels),
  Down  = lists:keyfind({X,  Y+1}, #map_pixel.xy, WallPixels),
  Left  = lists:keyfind({X-1,Y},   #map_pixel.xy, WallPixels),
  Right = lists:keyfind({X+1,Y},   #map_pixel.xy, WallPixels),
  case {Up, Left, Down, Right} of
    {#map_pixel{}, #map_pixel{},            _,            _} -> ?BARRIER_ORIENTATION_NW; 
    {           _, #map_pixel{}, #map_pixel{},            _} -> ?BARRIER_ORIENTATION_SW;
    {           _,         _,    #map_pixel{}, #map_pixel{}} -> ?BARRIER_ORIENTATION_SE;
    {#map_pixel{},         _,               _, #map_pixel{}} -> ?BARRIER_ORIENTATION_NE;
    _ -> ?Print("Bad pixel detected at (~p,~p)!\n"
                "Check to make sure that triangle and circle walls have only 2 connecting sides.\n",[X,Y]),
         throw({error, {bad_pixel}})
  end.

create_tile_image(Path, Pixels, Tiles, DeleteArtifacts, Descriptor) ->
  create_tile_image(Path, Pixels, Tiles, DeleteArtifacts, Descriptor, 2).
create_tile_image(Path, Pixels, Tiles, DeleteArtifacts, Descriptor, Scale) ->
  MakeDir = fun(Path) -> case file:make_dir(Path) of
                           ok -> ok;
                           {error, eexist} -> ok;
                           {error, Error} -> ?Print("Encountered error while making directory <~s>.", [Path]),
                                             throw({error, Error})
                         end end,
  FinalBitmap = composite_tiles(Pixels, Tiles),
  MapName = filename:basename(Path, ".png"),
  BuildArtifacts = filename:join("maps", ".build_artifacts"),
  BuildDirectory = filename:join(BuildArtifacts, MapName),

  MakeDir(BuildArtifacts),
  MakeDir(BuildDirectory),
  PreScaledImage = wxBitmap:convertToImage(FinalBitmap),
  wxBitmap:destroy(FinalBitmap),
  FinalImage = wxImage:scale(PreScaledImage,
                             round(wxImage:getWidth(PreScaledImage)*Scale),
                             round(wxImage:getHeight(PreScaledImage)*Scale)),
  SavePath = filename:join(BuildDirectory, map:file_name(MapName, Descriptor)),
  ?Print("Saving result to " ++ SavePath),
  true = wxImage:saveFile(FinalImage, SavePath),
  wxImage:destroy(PreScaledImage),
  wxImage:destroy(FinalImage),
  ImageBinary = case file:read_file(SavePath) of
                  {ok, Data} -> Data;
                  {error, Error} -> ?Print("Encountered error while reading file ~s.",[SavePath]),
                                    throw({error, image_read_error, Error})
                end,
  case DeleteArtifacts of
    true  -> file:delete(SavePath);
    false -> ok
  end,
  ImageBinary.

composite_tiles(Pixels, Tiles) ->
  #map_pixel{xy = {X,Y}} = lists:last(Pixels),
  ?Print("Building new bitmap ~px~p\n",[X+1,Y+1]),
  MapWidth = (X+1)*25,
  MapHeight = (Y+1)*25,
  Seq = create_seq(X+1, Y+1),
  Bitmap = wxBitmap:new(MapWidth, MapHeight),
  MemoryDC = wxMemoryDC:new(Bitmap),
  {#map_tile{bitmap = BaseTileBitmap},_} = find_tile({{6,0},0}, Tiles),
  ?Print("Laying floor layer"),
  lists:foreach(fun({A,B}) -> wxDC:drawBitmap(MemoryDC, BaseTileBitmap, {A*25, B*25}) end, Seq),
  Fun = fun(#map_pixel{tile_key = undefined}, AccTiles) -> AccTiles;
           (#map_pixel{xy = {A,B},
                       tile_key = TileKey}, AccTiles) ->
             {Tile, NewTiles} = find_tile(TileKey, AccTiles),
             #map_tile{bitmap = TileBitmap} = Tile,
             Mask = wxMask:new(TileBitmap, ?Mask),
             wxBitmap:setMask(TileBitmap, Mask),
             wxDC:drawBitmap(MemoryDC, TileBitmap, {A*25, B*25}, [{useMask, true}]),
             NewTiles end,
  ?Print("Laying top layer"),
  lists:foldl(Fun, Tiles, Pixels),
  Bitmap.

find_tile({{X,Y}, R} = Key, Tiles) ->
  case lists:keyfind(Key, #map_tile.xyr, Tiles) of
    false -> Tile = lists:keyfind({{X,Y},0}, #map_tile.xyr, Tiles),
             RotatedTile = rotate_tile(Tile, R),
             {RotatedTile, [RotatedTile | Tiles]};
    Tile  -> {Tile, Tiles}
  end.

rotate_tile(#map_tile{xyr = {{X,Y},0},
                      image = Image}, Rotate) ->
  RotatedImage =
    try
      Rot90 = wxImage:rotate90(Image),
      Rotate == 1 andalso throw({ok, Rot90}),

      Rot180 = wxImage:rotate90(Rot90),
      wxImage:destroy(Rot90),
      Rotate == 2 andalso throw({ok, Rot180}),

      Rot270 = wxImage:rotate90(Rot180),
      wxImage:destroy(Rot180),
      Rot270
    catch
      _:{ok, NewImage} -> NewImage
    end,
  #map_tile{xyr    = {{X,Y}, Rotate},
            image  = RotatedImage,
            bitmap = wxBitmap:new(RotatedImage)}.

create_seq(Width, Height) ->
  [{X, Y} || Y <- lists:seq(0, Height-1), X <- lists:seq(0, Width-1)].
