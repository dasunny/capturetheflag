-module(messaging).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([make_mails/2, make_mails_for_all_but/3, send/2]).
-export([convert_universe_update/2]). %% For testing

make_mails(Players, Message) ->
  Random = random:uniform(1000000),
  [?MakeMail(To, #idmail{mail = Message, id = Random}) || #player{player_id = To} <- Players].

make_mails_for_all_but(Players, IWorld, Message) ->
  Random = random:uniform(1000000),
  OtherPlayers = player:get_other_players(Players, IWorld),
  [?MakeMail(To, #idmail{mail = Message, id = Random}) || #player{player_id = To} <- OtherPlayers].

send(Players, Update) ->
  CleanUpdate = ?CLEANUP(Update),
  Random = random:uniform(1000000),
  Fun = fun(#player{player_id = To}) ->
                case convert_universe_update(CleanUpdate, To) of
                  undefined -> false;
                    Final -> {true, ?MakeMail(To, #idmail{mail = Final, id = Random})}
                end end,
  Out = lists:filtermap(Fun, Players),
  Out.

convert_universe_update(UniverseUpdate, MyId) ->
  Expanded = ?EXPAND(UniverseUpdate),
  ?COLLAPSE([convert_update_to_target(MyId, FlatUpdate) || FlatUpdate <- Expanded]).

convert_update_to_target(TargetId, {#universe.players, {id_update, {{#player.player_id, TargetId}, Update}}}) ->
  {#world.player, Update};
convert_update_to_target(_TargetId, {#universe.players, {id_update, {{#player.player_id, Id}, Update}}}) ->
  ?CLEANUP({#world.otherplayers, {id_update, {{#player.player_id, Id}, player:strip_private_data(Update)}}});
convert_update_to_target(_TargetId, {#universe.gamedata, Update}) ->
  {#world.gamedata, Update};
convert_update_to_target(_TargetId, Update) ->
  Update.

%% ====================================================================
%% Internal functions
%% ====================================================================


