-module(ie_comp_ctf_call_first).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_LAST},
                                    iuniverse = #type_info{init_info = ?INIT_LAST}}.

describe(Options) ->
  case ?OptionGetValue(call_first, Options) of
    undefined -> undefined;
    Callback  -> {"call_first", Callback}
  end.

init(#iState{state   = InitialIState,
             options = Options}) ->
  case ?OptionGetValue(call_first, Options) of
    undefined -> undefined;
    Callback  -> Callback(InitialIState),
                 ok
  end.

stop(IState) -> IState.

event(IState, _CallbackEvent) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================
