-module(ie_comp_ctf_remote_config).

-include("interactive_erlang.hrl").
-include("remote_config.hrl").
-include_lib("wx/include/wx.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_system_dictionary, ie_comp_net_world]},
                                    iuniverse = #type_info{init_info = [ie_comp_system_dictionary, ie_comp_net_universe]}}.

describe(Options) ->
  Setup = case ?OptionGetValue(remote_config_setup, Options) of
            undefined -> undefined;
            Callback1 -> {"remote_config_setup", Callback1}
          end,
  Callback = case ?OptionGetValue(remote_config, Options) of
               undefined -> undefined;
               Callback2 -> {"remote_config", Callback2}
             end,
  [Setup, Callback].

init(#iState{options = Options,
             type    = ?IUNIVERSE} = IState) ->
  SetupCallback = case ?OptionGetValue(remote_config_setup, Options) of
                    undefined -> undefined;
                    Callback  -> Callback
                  end,
  setup(IState, SetupCallback);
init(_IState) ->
  undefined.

stop(#iState{extra_data = #{remote_config := Frame}} = IState) ->
  wxFrame:hide(Frame),
  IState.

event(IState, _CallbackEvent) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

setup(#iState{extra_data = #{wx := Wx} = ExtraData} = IState, Callback) ->
  Frame = wxFrame:new(Wx, ?wxID_ANY, "Remote Config", [{style, ?wxDEFAULT_FRAME_STYLE}]),
  Panel = wxScrolledWindow:new(Frame),
  MainSizer = wxBoxSizer:new(?wxHORIZONTAL),
  Columns = Callback(),
  wxPanel:setSizer(Panel, MainSizer),
  add_columns(Panel, MainSizer, Columns),
  wxFrame:show(Frame),
  {ok, IState#iState{extra_data = ExtraData#{remote_config => Frame}}}.

add_columns(Panel, MainSizer, Columns) ->
  lists:foreach(fun(?Column(Title, Controls)) -> Sizer = case Title of
                                                           "" -> wxBoxSizer:new(?wxVERTICAL);
                                                           _  -> wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, Title}])
                                                         end,
                                                 wxSizer:add(MainSizer, Sizer),
                                                 add_controls(Panel, Sizer, Controls) end, Columns),
  ok.

add_controls(Panel, Sizer, Controls) ->
  lists:foreach(fun(?Section(Title, Controls)) -> SubSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, Title}]),
                                                  wxSizer:add(Sizer, SubSizer),
                                                  add_controls(Panel, SubSizer, Controls);
                   (Control)                   -> ItemSizer = make_control(Panel, Control),
                                                  wxSizer:add(Sizer, ItemSizer, [{flag, ?wxEXPAND},{proportion, 1}]) end, Controls),
  ok.

make_control(Panel, ?Radio(Title, Choices)) ->
  wxRadioBox:new(Panel, ?wxID_ANY, Title, ?wxDefaultPosition, ?wxDefaultSize,
                 Choices, [{majorDim, 1}, {style, ?wxHORIZONTAL}]);
make_control(Panel, ?Text(Title, Default)) ->
  Sizer = make_item_sizer(Title, Panel),
  Ctrl = wxTextCtrl:new(Panel, ?wxID_ANY, [{value, Default}]),
  wxSizer:add(Sizer, Ctrl, [{flag, ?wxEXPAND},{proportion, 1}]),
  Sizer;
make_control(Panel, ?CheckBox(Title, Choices)) ->
  Sizer = make_item_sizer(Title, Panel),
  lists:foreach(fun(Choice) -> Ctrl = wxCheckBox:new(Panel, ?wxID_ANY, Choice),
                               wxSizer:add(Sizer, Ctrl, [{flag, ?wxEXPAND},{proportion, 1}]) end, Choices),
  Sizer;
make_control(Panel, ?ListBox(Title, Choices)) ->
  Sizer = make_item_sizer(Title, Panel),
  ListBox = wxListBox:new(Panel, ?wxID_ANY, [{choices, Choices},
                                             {style, ?wxLB_MULTIPLE
                                                bor ?wxLB_NEEDED_SB
                                                bor ?wxLB_HSCROLL}]),
  wxSizer:add(Sizer, ListBox, [{flag, ?wxEXPAND},{proportion, 1}]),
  Sizer;
make_control(Panel, ?Spin(Title, Default, Min, Max)) ->
  Sizer = make_item_sizer(Title, Panel),
  Spin = wxSpinCtrl:new(Panel, [{initial, Default}]),
  wxSpinCtrl:setRange(Spin, Min, Max),
  wxSizer:add(Sizer, Spin, [{flag, ?wxEXPAND},{proportion, 1}]),
  Sizer.

make_item_sizer(Title, Panel) ->
  wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, [{label, Title}]).
