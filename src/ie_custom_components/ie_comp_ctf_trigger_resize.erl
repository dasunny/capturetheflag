-module(ie_comp_ctf_trigger_resize).

-include("interactive_erlang.hrl").
-include("idraw.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([resize/3, resize/5]).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_draw]},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(_Options) ->
  undefined.

init(_IState) ->
  ok.

stop(IState) -> IState.

event(#iState{extra_data = #{canvases := Canvases} = ExtraData} = IState,
      #callback_event{event_object = {resize, Pane, Size}}) ->
  case lists:keyfind(Pane, #canvas.name, Canvases) of
    false  -> IState;
    Canvas -> UpdatedCanvas = update_canvas(Canvas, Size),
              IState#iState{extra_data = ExtraData#{canvases => lists:keyreplace(Pane, #canvas.name,
                                                                                 Canvases, UpdatedCanvas)}}
  end.

resize(Pane, Width, Height) ->
  ?SendCallbackEvent(#callback_event{event_object = {resize, Pane, {Width, Height}}}).
resize(Pane, Width, Height, X, Y) ->
  ?SendCallbackEvent(#callback_event{event_object = {resize, Pane, {X, Y, Width, Height}}}).

%% ====================================================================
%% Internal functions
%% ====================================================================

update_canvas(#canvas{panel = Panel, pid = Pid, simple_canvas = Simple} = Canvas, Size) ->
  {Width, Height} = case Size of
                      {W, H} -> {W, H};
                      {_X, _Y, W, H} -> {W, H}
                    end,
  wxPanel:setSize(Panel, Size),
  ?Send(Pid, {resize, Width, Height}),
  Canvas#canvas{width  = Width,
                height = Height,
                simple_canvas = Simple#simple_canvas{width = Width,
                                                     height = Height}}.
