-module(ie_comp_ctf_pane_toggle).

-include("interactive_erlang.hrl").
-include("idraw.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([hide_pane/1, show_pane/1]).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_draw]},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(Options) ->
  Hiddens = [Pane || {start_hidden, Pane} <- Options],
  case Hiddens of
    []   -> undefined;
    Many -> {"start_hidden", "The following pane(s) will start hidden: ~s.",
             io_lib:format(lists:join(", ", ["~p" || _ <- Many]), [Many])}
  end.

init(#iState{options = Options}) ->
  [hide_pane(Pane) || {start_hidden, Pane} <- Options],
  ok.

stop(IState) -> IState.

event(#iState{extra_data = #{canvases := Canvases}} = IState, #callback_event{event_object = {Action, PaneName}}) ->
  case lists:keyfind(PaneName, #canvas.name, Canvases) of
    false -> ok;
    #canvas{panel = Panel} -> wxPanel:Action(Panel)
  end,
  IState.

hide_pane(Pane) ->
  ?SendCallbackEvent(#callback_event{event_object = {hide, Pane}}).

show_pane(Pane) ->
  ?SendCallbackEvent(#callback_event{event_object = {show, Pane}}).

%% ====================================================================
%% Internal functions
%% ====================================================================

