-module(perk_chooser).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([draw/2, key/3, mouse/4]).

-define(ThisCanvasWidth, ?CanvasWidth(?Canvas(?PANE_CHOOSING))).
-define(ThisCanvasHeight, ?CanvasHeight(?Canvas(?PANE_CHOOSING))).

-define(X0, (?ThisCanvasWidth div 2) - 75 -25 -150).
-define(X1, (?ThisCanvasWidth div 2) - 75).
-define(X2, (?ThisCanvasWidth div 2) + 100).

-define(BARRIER_PATH, "resources/icons/barrier.png").
-define(PAROLE_PATH,  "resources/icons/parole.png").
-define(RADAR_PATH,   "resources/icons/radar.png").
-define(SPRINT_PATH,  "resources/icons/sprint.png").
-define(STEALTH_PATH, "resources/icons/stealth.png").
-define(VAULT_PATH,   "resources/icons/vault.png").

-define(DESC_POSITION,    ?Posn((?ThisCanvasWidth div 2) - 375, 450)).

-define(BARRIER_DESC, "BARRIER - Drop a wall in front of your enemy!").
-define(PAROLE_DESC,  "PAROLE - Spend less time in jail!").
-define(RADAR_DESC,   "OVERSIGHT - See invisible players and other useful information!").
-define(SPRINT_DESC,  "LIGHTWEIGHT - Sprint forever!").
-define(STEALTH_DESC, "STEALTH - Turn invisible! (Can not use when holding the flag)").
-define(VAULT_DESC,   "VAULT - Jump over walls and other barriers!").

-define(BLUE_DESC, "BLUE TEAM - Fight for the glorious BLUE team!").
-define(AUTO_DESC, "AUTO-TEAM - Let the server pick your team for you!").
-define(RED_DESC,  "RED TEAM - Battle for the honorable RED team!").

-define(BARRIER,  {?BARRIER_PATH, ?Posn(?X0, 25),  ?BARRIER_DESC, ?PERK_BARRIER}).
-define(PAROLE,   {?PAROLE_PATH,  ?Posn(?X1, 25),  ?PAROLE_DESC,  ?PERK_PAROLE}).
-define(RADAR,    {?RADAR_PATH,   ?Posn(?X2, 25),  ?RADAR_DESC,   ?PERK_RADAR}).
-define(SPRINT,   {?SPRINT_PATH,  ?Posn(?X0, 200), ?SPRINT_DESC,  ?PERK_SPRINT}).
-define(STEALTH,  {?STEALTH_PATH, ?Posn(?X1, 200), ?STEALTH_DESC, ?PERK_STEALTH}).
-define(VAULT,    {?VAULT_PATH,   ?Posn(?X2, 200), ?VAULT_DESC,   ?PERK_VAULT}).

-define(BLUETEAM, {?BLUE,         ?Posn(?X0, 375), ?BLUE_DESC,    blue, "Blue Team"}).
-define(AUTOTEAM, {?GRAY,         ?Posn(?X1, 375), ?AUTO_DESC,    auto, "Auto-Team"}).
-define(REDTEAM,  {?RED,          ?Posn(?X2, 375), ?RED_DESC,     red,  "Red Team"}).

-define(ARROW, {"resources/util/right_arrow.png",
                "resources/util/right_arrow-w.png",
                "resources/util/right_arrow-g.png"}).

-define(ARROW_POSX, ?ThisCanvasWidth - 100).
-define(ARROW_POSY, ?ThisCanvasHeight - 100).
-define(ARROW_POS, ?Posn(?ARROW_POSX, ?ARROW_POSY)).

-define(FULLSET, [?BARRIER, ?PAROLE, ?RADAR, ?SPRINT, ?STEALTH, ?VAULT, ?BLUETEAM, ?AUTOTEAM, ?REDTEAM, ?ARROW]).

draw(#world{player = #player{player_team = Team,
                             player_perk = Perk},
            mouse  = Mouse},
     Canvas) ->
  Fun = fun(Elem, Acc) -> draw_item(Acc, Mouse, {Perk, Team}, Elem) end,
  C0 = ?DrawRectangle(Canvas, ?Posn(0, 0), ?CanvasWidth(Canvas), ?CanvasHeight(Canvas), ?BLACK),
  C1 = ?DrawRectangle(C0, ?DESC_POSITION, 750, 25, ?WHITE),
  lists:foldl(Fun, C1, ?FULLSET).

key(#world{player = #player{player_perk = undefined}} = World, " ", key_down) ->
  World;
key(#world{player = #player{player_team = undefined}} = World, " ", key_down) ->
  World;
key(#world{player = #player{player_name = Name,
                            player_perk = Perk,
                            player_team = Team}} = World, " ", key_down) ->
  ?MakePackage(World, {handshake, Name, Perk, Team});
key(World, _Key, _Type) ->
  World.

mouse(#world{player = #player{player_name = Name,
                              player_perk = Perk,
                              player_team = Team}} = World,
      X, Y, left_down) ->
  PerkPair = [{Perk, Pos} || {_, Pos, _, Perk} <- [?BARRIER, ?PAROLE, ?RADAR, ?SPRINT, ?STEALTH, ?VAULT]],
  TeamPair = [{Team, Pos} || {_, Pos, _, Team, _} <- [?BLUETEAM, ?AUTOTEAM, ?REDTEAM]],
  Fun1 = fun({Perk, ?Posn(Px, Py)}, Acc) -> case ?ISBETWEEN(Px, X, Px + 150) andalso ?ISBETWEEN(Py, Y, Py + 150) of
                                              true  -> Perk;
                                              false -> Acc end end,
  Fun2 = fun({Team, ?Posn(Px, Py)}, Acc) -> case ?ISBETWEEN(Px, X, Px + 150) andalso ?ISBETWEEN(Py, Y, Py + 50) of
                                              true  -> Team;
                                              false -> Acc end end,
  Perk2 = lists:foldl(Fun1, Perk, PerkPair),
  Team2 = lists:foldl(Fun2, Team, TeamPair),
  IsIB = ?ISBETWEEN(?ARROW_POSX, X, ?ARROW_POSX + 50) andalso ?ISBETWEEN(?ARROW_POSY, Y, ?ARROW_POSY + 50),
  Message = if IsIB andalso
                 (Perk2 /= undefined) andalso
                 (Team2 /= undefined) -> {handshake, Name, Perk2, Team2};
               true -> undefined end,
  ?MakePackage(?UPDATE(World, {#world.player, {many, [{set, {#player.player_perk, Perk2}},
                                                      {set, {#player.player_team, Team2}}]}}), Message);
mouse(World, _X, _Y, _) ->
  World.

%% ====================================================================
%% Internal functions
%% ====================================================================

draw_item(Canvas, Mouse, {Perk, _}, {Path, Position, Description, Atom}) ->
  Hovering = hovering(Mouse#mouse.position, Position, 150, 150),
  Selected = Perk == Atom,
  BackColor = case {Selected, Hovering} of
                {true, _}    -> ?GREEN;
                {_,    true} -> ?WHITE;
                _            -> ?BLACK
              end,
  C0 = ?DrawRectangle(Canvas, ?Translate(Position, -5, -5), 160, 160, BackColor),
  C1 = ?DrawImage(C0, Position, Path),
  draw_description(C1, Description, Hovering);
draw_item(Canvas, Mouse, {_, Team}, {Color, Position, Description, Atom, Label}) ->
  Hovering = hovering(Mouse#mouse.position, Position, 150, 50),
  Selected = Team == Atom,
  BackColor = case {Selected, Hovering} of
                {true, _}    -> ?GREEN;
                {_,    true} -> ?WHITE;
                _            -> ?BLACK
              end,
  Width = ?TextWidth(Label),
  C0 = ?DrawRectangle(Canvas, ?Translate(Position, -5, -5), 160, 60, BackColor),
  C1 = ?DrawRectangle(C0, Position, 150, 50, Color),
  C2 = ?DrawText(C1, ?Translate(Position, 75 - (Width div 2), 15), Label),
  draw_description(C2, Description, Hovering);
draw_item(Canvas, Mouse, {Perk, Team}, {NORMAL, WHITE, GREEN}) ->
  Hovering = hovering(Mouse#mouse.position, ?ARROW_POS, 75, 75),
  Selected = (Perk /= undefined) andalso (Team /= undefined),
  Path = case {Selected, Hovering} of
           {false,     _} -> NORMAL;
           {true,  false} -> WHITE;
           {true,   true} -> GREEN
         end,
  ?DrawImage(Canvas, ?ARROW_POS, Path).

draw_description(Canvas, _Text, false) -> Canvas;
draw_description(Canvas, Text, true) ->
  ?DrawText(Canvas, ?Translate(?DESC_POSITION, 15, 3), Text).

hovering(?Posn(MouseX, MouseY), ?Posn(ItemX, ItemY), Width, Height) ->
  ?ISBETWEEN(ItemX, MouseX, ItemX + Width) andalso
    ?ISBETWEEN(ItemY, MouseY, ItemY + Height).
