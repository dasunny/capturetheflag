-module(pane_status).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([draw/2, get_flag_path/1]).

draw(#world{player = #player{player_name = Name,
                             player_position = ?Posn(PlayerX, PlayerY),
                             player_perk = Perk,
                             player_team = Team,
                             has_flag    = HasFlag},
            otherplayers = OPlayers,
            gamedata = #gamedata{map_data = #map_data{name = MapName}}}, Canvas) ->
  Width  = ?CanvasWidth(Canvas),
  Height = ?CanvasHeight(Canvas),
  MyColor = utilities:get_color(Team),
  TheirColor = utilities:get_other_team(Team),
  C0 = ?DrawRectangle(Canvas, ?Posn(0, 0), Width, Height, ?BLACK),
%%   C1 = ?DrawImage(C0, ?Posn(0, 0), get_path(light, Perk)),
  MiniMap = map:file_location(MapName, "mini"),
  Displacement = panes:calculate_scroll_displacement({Width, Width}, {?ImageWidth(MiniMap),
                                                                      ?ImageHeight(MiniMap)}, ?Posn(PlayerX div 8,
                                                                                                    PlayerY div 8), false),
  C1 = ?DrawImage(C0, ?Translate(?Posn(0,0), Displacement), MiniMap),
  C1a = ?DrawCircle(C1, ?Posn(Width div 2, Width div 2), 2, utilities:get_color(Team)),
  C1b = ?DrawRectangle(C1a, ?Posn(0, Width), Width, Height, ?WHITE),
  C2 = ?DrawText(C1b, ?Posn(5, 155), Name),
  C3 = case HasFlag of
         true  -> ?DrawImage(C2, ?Posn(Width - 50, 150), get_flag_path(Team));
         false -> C2
       end,
  Base = 210,
  C4 = ?DrawLine(C3, ?Posn(10, 205), ?Posn(Width - 10, 205), MyColor, 3),
  {C5, _, _, Offset1} = lists:foldl(fun draw_oplayer/2, {C4, Team, Base, 0}, OPlayers),
  Base2 = Base + Offset1*55,
  C6 = ?DrawLine(C5, ?Posn(10, Base2), ?Posn(Width  - 10, Base2), utilities:get_color(TheirColor), 3),
  {C7, _, _, _Offset2} = lists:foldl(fun draw_oplayer/2, {C6, TheirColor, Base2 + 10, 0}, OPlayers),
  C7.

%% ====================================================================
%% Internal functions
%% ====================================================================

draw_oplayer(#player{player_name      = Name,
                     player_perk      = Perk,
                     has_flag  = HasFlag,
                     is_jailed = IsJailed,
                     player_team      = Team},
             {Canvas, Team, Base, Offset}) ->
  Y = Base + (55*Offset),
  C0 = ?DrawImage(Canvas, ?Posn(5, Y), get_path(small, Perk)),
  {C1, XOff} = case {HasFlag, IsJailed} of
                 {true, _} -> {?DrawImage(C0, ?Posn(60, Y), get_flag_path(utilities:get_other_team(Team))), 55};
                 {_, true} -> {?DrawImage(C0, ?Posn(60, Y), "resources/util/jailed.png"), 55};
                 _         -> {C0, 0}
               end,
  C2 = ?DrawText(C1, ?Posn(60 + XOff, Y + 20), Name, ?BLACK),
  {C2, Team, Base, Offset + 1};
draw_oplayer(_, Acc) -> Acc.
 
get_path(light, ?PERK_BARRIER) -> "resources/icons-light/barrier.png";
get_path(light, ?PERK_SPRINT)  -> "resources/icons-light/sprint.png";
get_path(light, ?PERK_RADAR)   -> "resources/icons-light/radar.png";
get_path(light, ?PERK_PAROLE)  -> "resources/icons-light/parole.png";
get_path(light, ?PERK_STEALTH) -> "resources/icons-light/stealth.png";
get_path(light, ?PERK_VAULT)   -> "resources/icons-light/vault.png";

get_path(small, ?PERK_BARRIER) -> "resources/icons-small/barrier.png";
get_path(small, ?PERK_SPRINT)  -> "resources/icons-small/sprint.png";
get_path(small, ?PERK_RADAR)   -> "resources/icons-small/radar.png";
get_path(small, ?PERK_PAROLE)  -> "resources/icons-small/parole.png";
get_path(small, ?PERK_STEALTH) -> "resources/icons-small/stealth.png";
get_path(small, ?PERK_VAULT)   -> "resources/icons-small/vault.png".

get_flag_path(blue) -> "resources/flags/blueflag.png";
get_flag_path(red)  -> "resources/flags/redflag.png";
get_flag_path(gray) -> "resources/flags/grayflag.png".
