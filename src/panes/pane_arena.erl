-module(pane_arena).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([draw/2]).

draw(World, Canvas) ->
  #world{arena_size = ArenaSize,
         gamedata = #gamedata{map_data = #map_data{name = MapName,
                                                   size = MapSize}},
         player = #player{is_radar = RadarOn,
                          player_position = PlayerPosition,
                          player_team = Team}} = World,
  Displacement = panes:calculate_scroll_displacement(ArenaSize, MapSize, PlayerPosition, true),
  DisplaceFun = fun(Posn) -> ?Translate(Posn, Displacement) end,
  RenderOpts = #render_options{client_team = Team,
                               displace_fun = DisplaceFun,
                               displacement = Displacement,
                               radar_on = RadarOn},
  MapPath = case RadarOn of
              true  -> map:file_location(MapName, "radar");
              false -> map:file_location(MapName, "arena")
            end,
  C1 = ?DrawImage(Canvas, DisplaceFun(?Posn(0,0)), MapPath),
%%   C2 = ?DrawLine(C1, DisplaceFun(?Posn((?ARENAWIDTH div 2) - 2, 0)),
%%                  DisplaceFun(?Posn((?ARENAWIDTH div 2) - 2, ?ARENAHEIGHT)), ?BLUE, 3),
%%   C3 = ?DrawLine(C2, DisplaceFun(?Posn((?ARENAWIDTH div 2) + 2, 0)),
%%                  DisplaceFun(?Posn((?ARENAWIDTH div 2) + 2, ?ARENAHEIGHT)), ?RED, 3),
  C2 = world:draw(World, C1, RenderOpts),
  case RadarOn of
    true  -> #world{arena_size = Size} = World,
             C3 = ?DrawImage(C2, ?Posn(0,0), "resources/radar/overlay1.png", [{opacity, 0.5},
                                                                              {resize, Size}]),
             ?DrawImage(C3, ?Posn(0,0), "resources/radar/overlay2.png", [{mask, ?WHITE},
                                                                         {resize, Size}]);
    false -> C2
  end.
