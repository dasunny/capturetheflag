-module(pane_hud).

-include("capturetheflag.hrl").
-include("timer.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([draw/2]).

draw(#world{gamedata = #gamedata{limit_time    = TimeLimit,
                                 limit_capture = CapLimit,
                                 score_blue    = BlueScore,
                                 score_red     = RedScore,
                                 flag_blue     = BlueFlag,
                                 flag_red      = RedFlag},
            player = #player{barrier             = Barrier,
                             is_invisible        = IsInvis,
                             is_jailed           = IsJailed,
                             player_perk         = Perk,
                             player_team         = Team,
                             timer_exhaustion    = ExhTimer,
                             timer_invisibility  = InvisTimer,
                             timer_jail          = JailTimer,
                             timer_sprint        = SprintTimer}}, Canvas) ->
  Width  = ?CanvasWidth(Canvas),
  Height = ?CanvasHeight(Canvas),
  C0 = ?DrawRectangle(Canvas, ?Posn(0,0), Width, Height, ?GRAY),
  C1 = utilities:draw_gauge(C0, ?GREEN, ?RED, 250, 25,
                            ?TIMER_GETVALUE(SprintTimer),
                            ?GETGAMERULE(#gamerule.health_sprint),
                            ?Posn(0,0),
                            not ?TIMER_ISEXPIRED(ExhTimer)),
  C2 = case Perk of
         ?PERK_STEALTH -> utilities:draw_gauge(C1, ?CYAN, ?COLOR(0,128,128), 250, 25,
                                               ?TIMER_GETVALUE(InvisTimer), ?GETGAMERULE(#gamerule.invis_max_health),
                                               ?Posn(0, 25),
                                               (?TIMER_GETVALUE(InvisTimer) < ?GETGAMERULE(#gamerule.invis_min_health)) andalso
                                                 (not IsInvis));
         ?PERK_BARRIER -> utilities:draw_gauge(C1, ?YELLOW, ?ORANGE, 250, 25,
                                               ?TIMER_GETVALUE(Barrier#barrier.timer), ?GETGAMERULE(#gamerule.health_barrier),
                                               ?Posn(0, 25), Barrier#barrier.is_active);
         _ -> C1 end,
  {Front1, Front2, Back1, Back2, CurValue1, CurValue2} =
    case Team of
      blue -> {?BLUE,  ?RED,   ?DARKBLUE, ?DARKRED,  BlueScore, RedScore};
      red  -> {?RED,   ?BLUE,  ?DARKRED,  ?DARKBLUE, RedScore,  BlueScore};
      _    -> {?WHITE, ?WHITE, ?WHITE,    ?WHITE,    0,         0}
    end,
  C3a = utilities:draw_gauge(C2, Front1, Back1, (Width - 130) div 2, 10, CurValue1, CapLimit, ?Posn(0, 50), false),
  C3b = utilities:draw_reverse_gauge(C3a, Front2, Back2, (Width - 130) div 2, 10, CurValue2, CapLimit, ?Posn((Width - 130) div 2, 50), false),
  C4 = utilities:draw_gauge(C3b, ?YELLOW, ?DARKGRAY, Width, 15, TimeLimit, ?GETGAMERULE(#gamerule.limit_time), ?Posn(0, 60), false),
  C5 = case Team of
         blue -> draw_flags(C4, BlueFlag, RedFlag, ?BLUE, ?RED);
         red  -> draw_flags(C4, RedFlag, BlueFlag, ?RED, ?BLUE);
         _    -> C4
       end,
  C6 = case IsJailed of
         true -> BaseJailTime = ?GETGAMERULE(#gamerule.time_jail),
%%                  JailTime = case Perk of
%%                               ?PERK_PAROLE -> ?GETGAMERULE(#gamerule.time_parole);
%%                               _            -> BaseJailTime
%%                             end,
                 utilities:draw_gauge(C5, ?COLOR(255,128,0), ?RED, Width - 600, 50,
                                      (?TIMER_GETVALUE(JailTimer)), BaseJailTime, ?Posn(300,0), false);
         _   -> C5
       end,
  C7 = case IsJailed of
         true -> C6a = ?DrawImage(C6, ?Posn(250, 0), "resources/util/jailed.png"),
                 ?DrawImage(C6a, ?Posn(Width - 300, 0), "resources/util/jailed.png");
         _    -> C6
       end,
  C7.

draw_flags(Canvas, #flag{team = T1, state = S1}, #flag{team = T2, state = S2}, Co1, Co2) ->
  Width = ?CanvasWidth(Canvas),
  XBase = Width - 130,
  C0 = ?DrawRectangle(Canvas, ?Posn(XBase, 0), 60, 60, Co1),
  C1 = case S1 of
         home -> ?DrawImage(C0, ?Posn(XBase + 5, 5), pane_status:get_flag_path(T1));
         _    -> ?DrawImage(C0, ?Posn(XBase + 5, 5), pane_status:get_flag_path(gray))
       end,
  C2 = ?DrawRectangle(C1, ?Posn(XBase + 70, 0), 60, 60, Co2),
  C3 = case S2 of
         home -> ?DrawImage(C2, ?Posn(XBase + 75, 5), pane_status:get_flag_path(T2));
         _    -> ?DrawImage(C2, ?Posn(XBase + 75, 5), pane_status:get_flag_path(gray))
       end,
  C3.
