-module(panes).

-include("capturetheflag.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([calculate_scroll_displacement/4, get_starting_panes/0, resize_pane/2, switch_view/1]).

-define(STATUSWIDTH,  200).
-define(STATUSHEIGHT, 600).
-define(HUDWIDTH,     1000).
-define(HUDHEIGHT,    100).

calculate_scroll_displacement({WindowWidth, WindowHeight} = _WindowSize,
                              {ImageWidth, ImageHeight} = _ImageSize,
                              ?Posn(FocusX, FocusY) = _FocusPoint,
                              AllowOverscroll) ->
  XDisplacement = -calculate_scroll_displacement_int(ImageWidth,  WindowWidth,  FocusX, AllowOverscroll),
  YDisplacement = -calculate_scroll_displacement_int(ImageHeight, WindowHeight, FocusY, AllowOverscroll),
  ?Posn(XDisplacement, YDisplacement).

calculate_scroll_displacement_int(ImageDimension, WindowDimension, _FocusCoordinate, true)
  when ImageDimension =< WindowDimension -> (ImageDimension - WindowDimension) div 2;
calculate_scroll_displacement_int(_ImageDimension, WindowDimension, FocusCoordinate, true)
  when FocusCoordinate < (WindowDimension div 2) -> 0;
calculate_scroll_displacement_int(ImageDimension, WindowDimension, FocusCoordinate, true)
  when FocusCoordinate > (ImageDimension - (WindowDimension div 2)) -> ImageDimension - WindowDimension;
calculate_scroll_displacement_int(_ImageDimension, WindowDimension, FocusCoordinate, _AllowOverScroll) ->
  FocusCoordinate - (WindowDimension div 2).

get_starting_panes() ->
  [{?PANE_CHOOSING, ?GAMEWIDTH, ?GAMEHEIGHT,    0,   0},
   {?PANE_ARENA,          1000,         500,    0,   0},
   {?PANE_HUD,            1000,         100,    0, 500},
   {?PANE_STATUS,          200, ?GAMEHEIGHT, 1000,   0},
   {?PANE_PREGAME,  ?GAMEWIDTH, ?GAMEHEIGHT,    0,   0},
   {?PANE_POSTGAME, ?GAMEWIDTH, ?GAMEHEIGHT,    0,   0}].

%% {W, H, X, Y}
resize_pane(?PANE_CHOOSING, {Width, Height}) -> {Width, Height, 0, 0};
resize_pane(?PANE_PREGAME,  {Width, Height}) -> {Width, Height, 0, 0};
resize_pane(?PANE_ARENA,    {Width, Height}) -> {Width - ?STATUSWIDTH, Height - ?HUDHEIGHT, 0, 0};
resize_pane(?PANE_STATUS,   {Width, Height}) -> {?STATUSWIDTH, Height, Width - ?STATUSWIDTH, 0};
resize_pane(?PANE_HUD,      {Width, Height}) -> {Width - ?STATUSWIDTH, ?HUDHEIGHT, 0, Height - ?HUDHEIGHT};
resize_pane(?PANE_POSTGAME, {Width, Height}) -> {Width, Height, 0, 0}.

switch_view(?PHASE_CHOOSING) ->
  hide_show([?PANE_CHOOSING]);
switch_view(?PHASE_PREGAME) ->
  hide_show([?PANE_PREGAME]);
switch_view(?PHASE_INGAME) ->
  hide_show([?PANE_ARENA, ?PANE_HUD, ?PANE_STATUS]);
switch_view(?PHASE_POSTGAME) ->
  hide_show([?PANE_POSTGAME]).

%% ====================================================================
%% Internal functions
%% ====================================================================

hide_show(Show) ->
  [?HidePane(Pane) || Pane <- [?PANE_CHOOSING,
                               ?PANE_ARENA,
                               ?PANE_HUD,
                               ?PANE_STATUS,
                               ?PANE_PREGAME,
                               ?PANE_POSTGAME]],
  [?ShowPane(Pane) || Pane <- Show].
